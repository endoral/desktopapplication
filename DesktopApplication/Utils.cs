﻿using System;
using System.Runtime.InteropServices;

namespace DesktopApplication
{
    class Utils
    {
        const uint MF_ENABLED = 0x0U;
        const uint MF_DISABLED = 0x2U;

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr GetSystemMenu(
            IntPtr hWnd,
            bool bRevert
            );

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr EnableMenuItem(
            IntPtr hWnd,
            uint uIDEnableItem,
            uint uEnable
            );

        [DllImport("user32.dll", SetLastError = true)]
        static extern uint GetMenuItemID(
            IntPtr hWnd,
            uint nPos
            );

        public static void SetCrossActive(IntPtr handle, bool enabled)
        {
            IntPtr hMenu = GetSystemMenu(/*this.Handle*/handle, false);

            uint menuID = 0U;

            if (hMenu != IntPtr.Zero)
            {
                menuID = GetMenuItemID(hMenu, 6U);
                EnableMenuItem(hMenu, menuID, enabled ? MF_ENABLED : MF_DISABLED);
            }
        }
    }
}
