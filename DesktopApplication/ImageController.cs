﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace DesktopApplication {
    sealed class ImageController {
        private static Rectangle proportionalResizedRect(Image image, int toSize) {
            int maxRatio = Math.Max(image.Size.Width, image.Size.Height);
            Point zeroPoint = new Point(0, 0);

            if (maxRatio < toSize) {
                return new Rectangle(zeroPoint, image.Size);
            }

            double newRatio = Convert.ToDouble(toSize) / Convert.ToDouble(maxRatio);
            int newWidth = Convert.ToInt32(Convert.ToDouble(image.Size.Width) * newRatio);
            int newHeight = Convert.ToInt32(Convert.ToDouble(image.Size.Height) * newRatio);
            Size newSize = new Size(newWidth, newHeight);

            return new Rectangle(zeroPoint, newSize);
        }

        public static Image resizedImage(Image image, int toSize) {
            Rectangle newRectangle = ImageController.proportionalResizedRect(image, toSize);
            Bitmap resizedImage = new Bitmap(newRectangle.Size.Width, newRectangle.Size.Height);
            Graphics graphics = Graphics.FromImage(resizedImage);
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.DrawImage(image, newRectangle);
            graphics.Dispose();

            try {
                return (Image)resizedImage.Clone();
            }
            finally {
                resizedImage.Dispose();
                resizedImage = null;
                graphics = null;
            }
        }
    }
}
