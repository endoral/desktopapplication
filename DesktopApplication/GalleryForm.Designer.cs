﻿namespace DesktopApplication
{
    partial class GalleryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GalleryForm));
            this.picBoxCMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.показатьВПапкеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.установитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.picBoxCMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // picBoxCMenu
            // 
            this.picBoxCMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.показатьВПапкеToolStripMenuItem,
            this.toolStripMenuItem1,
            this.установитьToolStripMenuItem});
            this.picBoxCMenu.Name = "picBoxCMenu";
            this.picBoxCMenu.Size = new System.Drawing.Size(169, 54);
            // 
            // показатьВПапкеToolStripMenuItem
            // 
            this.показатьВПапкеToolStripMenuItem.Name = "показатьВПапкеToolStripMenuItem";
            this.показатьВПапкеToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.показатьВПапкеToolStripMenuItem.Text = "Показать в папке";
            this.показатьВПапкеToolStripMenuItem.Click += new System.EventHandler(this.показатьВПапкеToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(165, 6);
            // 
            // установитьToolStripMenuItem
            // 
            this.установитьToolStripMenuItem.Name = "установитьToolStripMenuItem";
            this.установитьToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.установитьToolStripMenuItem.Text = "Установить";
            this.установитьToolStripMenuItem.Click += new System.EventHandler(this.установитьToolStripMenuItem_Click);
            // 
            // GalleryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(600, 462);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(616, 500);
            this.Name = "GalleryForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Галерея";
            this.Resize += new System.EventHandler(this.GalleryForm_Resize);
            this.picBoxCMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip picBoxCMenu;
        private System.Windows.Forms.ToolStripMenuItem показатьВПапкеToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem установитьToolStripMenuItem;



    }
}