﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace DesktopApplication {
    static class Program {
        private static bool _firstCopy;

        private static Mutex _mutex = new Mutex(
            true,
            Application.ProductName + "." + Application.CompanyName,
            out _firstCopy
        );

        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main() {
            LogSystem.Log();
            LogSystem.Log("Инициализация запуска приложения...");

            if (!Debugger.IsAttached) {
                if (_firstCopy) {
                    try {
                        InitializeRun();
                    }
                    catch (SystemException ex) {
                        LogSystem.Log(ex.Message);
                    }
                }
                else {
                    LogSystem.Log("Попытка запуска более чем одной копии приложения!");
                    LogSystem.Log("Директория: " + Application.ExecutablePath);
                    LogSystem.Log();
                }
            }
            else {
                try {
                    if (!_firstCopy) {
                        MessageBox.Show(
                            "Программа запущена в режиме отладки, но уже имеется запущенная копия приложения. Для корректной работы отладки следует закрыть установленную копию приложения!",
                            "Предупреждение!",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Warning
                        );
                    }

                    InitializeRun();
                }
                catch (SystemException ex) {
                    LogSystem.Log(ex.Message);
                }
            }
        }

        private static void InitializeRun() {
            LogSystem.Log("Запуск приложения...");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new mainWnd());
        }
    }
}
