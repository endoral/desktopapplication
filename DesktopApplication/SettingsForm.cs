﻿using System;
using System.Windows.Forms;
using Microsoft.Win32;

namespace DesktopApplication
{
    public partial class SettingsForm : Form
    {
        #region Constructors

        public SettingsForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Fields

        private static SettingsForm _instance;

        #endregion

        #region Methods

        /// <summary>
        /// Показать модальное окно настроек.
        /// </summary>
        public static void ShowModal()
        {
            if (_instance == null)
            {
                _instance = new SettingsForm();
            }

            _instance.InitializeData();
            _instance.ShowDialog();
        }

        private void InitializeData()
        {
            InterfaceUpdate();

            autorunCh.Checked = SettingsSystem.Instance.checkAutorunState;
            randomStartCh.Checked = SettingsSystem.Instance.checkStartWithRandomImageState;

            trayCh.Checked = SettingsSystem.Instance.checkMinimizeToTrayState;
            alwaysTrayCh.Checked = SettingsSystem.Instance.checkAlwaysInTrayState;
            dontAskWnenExitCh.Checked = SettingsSystem.Instance.dontAskWhenCloseState;
            stickyBoundsCh.Checked = SettingsSystem.Instance.checkStickyBoundsState;

            autochangeCh.Checked = SettingsSystem.Instance.checkAutochangeImageState;
            randomCh.Checked = SettingsSystem.Instance.checkRandomImageState;

            intNum.Value = (SettingsSystem.Instance.numIntValue > 0 ?
                SettingsSystem.Instance.numIntValue / 60000 : 1);

            searchInChildCh.Checked = SettingsSystem.Instance.searchInSubfoldersState;
            autoAddCh.Checked = SettingsSystem.Instance.autoAddFilesState;

            closeByX.Checked = !SettingsSystem.Instance.minimizeByX;
            minimizeByX.Checked = SettingsSystem.Instance.minimizeByX;
        }

        private void InterfaceUpdate()
        {
            randomStartCh.Enabled = SettingsSystem.Instance.checkStartWithRandomImageEnabled;

            autochangeCh.Enabled = SettingsSystem.Instance.checkAutochangeImageEnabled;
            randomCh.Enabled = SettingsSystem.Instance.checkRandomImageEnabled;

            intNum.Enabled = SettingsSystem.Instance.numIntEnabled;
            intLb.Enabled = SettingsSystem.Instance.labelMarkIntEnabled;
            intFormatLb.Enabled = SettingsSystem.Instance.labelMarkIntMinutesEnabled;
        }

        public static void Autorun(bool active)
        {
            if (!System.Diagnostics.Debugger.IsAttached)
            {
                LogSystem.Log("Изменение статуса автозапуска приложения.");
                LogSystem.Log("Статус: " + active + ".");

                RegistryKey vAutorunKey = null;

                try
                {
                    LogSystem.Log("Открытие ключа реестра по указанному адресу...");

                    LogSystem.Log(
                        "Адрес ключа: \""
                        + @"HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Run\" +
                        "\";");

                    vAutorunKey =
                        Registry.CurrentUser.OpenSubKey
                        (@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run\", true);

                    LogSystem.Log("Указанный адрес успешно открыт!");
                }
                catch (SystemException ex)
                {
                    LogSystem.Log(ex.Message);
                }

                if (active)
                {
                    try
                    {
                        LogSystem.Log("Занесение значения в указанный ключ...");
                        LogSystem.Log(
                            "Ключ: \"" +
                            "DesktopApplication" + "\"; " +
                            "Значение: \"" +
                            Application.ExecutablePath + "\";");

                        vAutorunKey.SetValue(
                            "DesktopApplication",
                            Application.ExecutablePath);

                        SettingsSystem.Instance.checkAutorunState = active;

                        LogSystem.Log("Значение успешно занесено в указанный ключ!");
                    }
                    catch (SystemException ex)
                    {
                        LogSystem.Log(ex.Message);
                    }
                }
                else
                {
                    try
                    {
                        LogSystem.Log("Удаление значения в указанном ключе...");

                        LogSystem.Log(
                           "Ключ: \"" +
                           "DesktopApplication" + "\";");

                        vAutorunKey.DeleteValue("DesktopApplication", false);

                        SettingsSystem.Instance.checkAutorunState = active;

                        LogSystem.Log("Значение в указанном ключе успешно удалено!");
                    }
                    catch (SystemException ex)
                    {
                        LogSystem.Log(ex.Message);
                    }
                }

                try
                {
                    LogSystem.Log("Закрытие ключа...");
                    vAutorunKey.Close();
                }
                catch (SystemException ex)
                {
                    LogSystem.Log(ex.Message);
                }
            }
            else
            {
                if (active)
                {
                    MessageBox.Show(
                        "В режиме отладки функция автозапуска недоступна!",
                        "Внимание!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }
            }

            SettingsSystem.SaveData();
        }

        #endregion

        #region Events

        /// <summary>
        /// Показать исполняемый файл приложения в проводнике.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openInFolderBtn_Click(object sender, EventArgs e)
        {
            mainWnd.openFolder(Application.ExecutablePath, true);
        }

        /// <summary>
        /// Автозапуск приложения.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void autorunCh_CheckedChanged(object sender, EventArgs e)
        {
            SettingsForm.Autorun(autorunCh.Checked);

            if (System.Diagnostics.Debugger.IsAttached)
            {
                autorunCh.Checked = false;
            }
        }

        /// <summary>
        /// Случайное изображение при старте программы.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void randomStartCh_CheckedChanged(object sender, EventArgs e)
        {
            SettingsSystem.Instance.checkStartWithRandomImageState = randomStartCh.Checked;

            SettingsSystem.SaveData();
        }

        /// <summary>
        /// Сворачивание приложения в трей.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void trayCh_CheckedChanged(object sender, EventArgs e)
        {
            SettingsSystem.Instance.checkMinimizeToTrayState = trayCh.Checked;

            SettingsSystem.SaveData();
        }

        /// <summary>
        /// Всегда отображать программу в трее.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void alwaysTrayCh_CheckedChanged(object sender, EventArgs e)
        {
            LogSystem.Log("Изменение статуса постоянного нахождения в трее.");
            LogSystem.Log("Статус: " + alwaysTrayCh.Checked + ".");

            SettingsSystem.Instance.checkAlwaysInTrayState = alwaysTrayCh.Checked;
            mainWnd.Instance.trayIcon.Visible = alwaysTrayCh.Checked;

            LogSystem.Log("Статус видимости иконки: " + mainWnd.Instance.trayIcon.Visible + ".");

            SettingsSystem.SaveData();
        }

        /// <summary>
        /// Не спрашивать при закрытии приложения.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dontAskWnenExitCh_CheckedChanged(object sender, EventArgs e)
        {
            SettingsSystem.Instance.dontAskWhenCloseState = dontAskWnenExitCh.Checked;

            SettingsSystem.SaveData();
        }

        /// <summary>
        /// Приклеивать основное окно программы к краям экрана.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void stickyBoundsCh_CheckedChanged(object sender, EventArgs e)
        {
            SettingsSystem.Instance.checkStickyBoundsState = stickyBoundsCh.Checked;

            SettingsSystem.SaveData();
        }

        /// <summary>
        /// Автосмена изображений.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void autochangeCh_CheckedChanged(object sender, EventArgs e)
        {
            SettingsSystem.Instance.checkAutochangeImageState = autochangeCh.Checked;

            LogSystem.Log("Изменение статуса автосмены изображения.");

            if (autochangeCh.Checked)
            {
                LogSystem.Log("Статус: " + autochangeCh.Checked + ".");

                SettingsSystem.Instance.checkRandomImageEnabled = true;
                SettingsSystem.Instance.numIntEnabled = true;
                SettingsSystem.Instance.labelMarkIntEnabled = true;
                SettingsSystem.Instance.labelMarkIntMinutesEnabled = true;

                if (!mainWnd.Instance.imgAutoChangeThread.IsBusy)
                {
                    SettingsSystem.Instance.checkAutochangeImageState = true;
                    LogSystem.Log(
                        "Поле настроек: " +
                        SettingsSystem.Instance.checkAutochangeImageState + ".");

                    try
                    {
                        LogSystem.Log("Запуск работы фонового потока...");
                        mainWnd.Instance.imgAutoChangeThread.RunWorkerAsync(null);
                    }
                    catch (SystemException ex)
                    {
                        LogSystem.Log(ex.Message);
                    }
                }
                else
                {
                    LogSystem.Log("Фоновый поток занят!");
                }
            }
            else
            {
                LogSystem.Log("Статус: " + autochangeCh.Checked + ".");

                SettingsSystem.Instance.checkRandomImageEnabled = false;
                SettingsSystem.Instance.numIntEnabled = false;
                SettingsSystem.Instance.labelMarkIntEnabled = false;
                SettingsSystem.Instance.labelMarkIntMinutesEnabled = false;

                SettingsSystem.Instance.checkRandomImageState = false;
                randomCh.Checked = SettingsSystem.Instance.checkRandomImageState;

                SettingsSystem.Instance.checkAutochangeImageState = false;
                LogSystem.Log(
                    "Поле настроек: " +
                    SettingsSystem.Instance.checkAutochangeImageState + ".");

                try
                {
                    LogSystem.Log("Прекращение работы фонового потока...");
                    mainWnd.Instance.imgAutoChangeThread.CancelAsync();
                }
                catch (SystemException ex)
                {
                    LogSystem.Log(ex.Message);
                }
            }

            InterfaceUpdate();

            SettingsSystem.SaveData();
        }

        /// <summary>
        /// Случайное изображение при автосмене.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void randomCh_CheckedChanged(object sender, EventArgs e)
        {
            SettingsSystem.Instance.checkRandomImageState = randomCh.Checked;

            SettingsSystem.SaveData();
        }

        /// <summary>
        /// Изменение интервала автосмены изображений кнопками компонента.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void intNum_ValueChanged(object sender, EventArgs e)
        {
            LogSystem.Log("Новое значение интервала автосмены изображений.");

            SettingsSystem.Instance.numIntValue = intNum.Value * 60000;

            LogSystem.Log(
                "Значение: " + intNum.Value +
                "; Поле настроек: " + SettingsSystem.Instance.numIntValue + ".");

            SettingsSystem.SaveData();
        }

        /// <summary>
        /// Изменение интервала автосмены изображений с клавиатуры.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void intNum_KeyUp(object sender, KeyEventArgs e)
        {
            LogSystem.Log("Изменение значения интервала автосмены изображений кнопками клавиатуры.");

            SettingsSystem.Instance.numIntValue = intNum.Value * 60000;

            LogSystem.Log(
                "Значение: " + intNum.Value +
                "; Поле настроек: " + SettingsSystem.Instance.numIntValue + ".");

            SettingsSystem.SaveData();
        }

        /// <summary>
        /// Поиск файлов в дочерних папках.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchInChildCh_CheckedChanged(object sender, EventArgs e)
        {
            SettingsSystem.Instance.searchInSubfoldersState = searchInChildCh.Checked;

            SettingsSystem.SaveData();
        }

        /// <summary>
        /// Автодобавление файлов.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void autoAddCh_CheckedChanged(object sender, EventArgs e)
        {
            SettingsSystem.Instance.autoAddFilesState = autoAddCh.Checked;

            SettingsSystem.SaveData();
        }

        private void closeByX_CheckedChanged(object sender, EventArgs e)
        {
            SettingsSystem.Instance.minimizeByX = false;

            SettingsSystem.SaveData();
        }

        private void minimizeByX_CheckedChanged(object sender, EventArgs e)
        {
            SettingsSystem.Instance.minimizeByX = true;

            SettingsSystem.SaveData();
        }

        #endregion
    }
}
