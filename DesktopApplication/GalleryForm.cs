﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;

namespace DesktopApplication {
    public partial class GalleryForm: Form {
        #region Constructors

        public GalleryForm() {
            InitializeComponent();
        }

        #endregion

        #region Fields

        private static GalleryForm _instance;
        private Form _parent;
        private Panel _panelBox;

        readonly private Point _indent = new Point(10, 10);
        private const int _previewWidth = 180;
        private const int _previewHeight = 120;
        private const int _previewsVerticalSpace = 10;
        private const int _previewsHorizontalSpace = 10;
        private const int _scrollWidth = 16;

        private int _currentPreviewsHorizontalSpace = 10;

        private int _previewsInLine = 3;
        private int _currentPreviewsInLine = 0;

        private Dictionary<string, Image> _previews = new Dictionary<string, Image>();

        #endregion

        #region Methods

        /// <summary>
        /// Показать модальное окно галереи.
        /// </summary>
        public static void ShowModal(Form parent, int x = -1, int y = -1) {
            if (_instance == null) {
                _instance = new GalleryForm();
                _instance.InitializeData();
                _instance._parent = parent;
            }

            if (x != -1 && y != -1) {
                _instance.StartPosition = FormStartPosition.Manual;
                _instance.Location = new Point(x, y);
            }
            else {
                _instance.StartPosition = FormStartPosition.CenterScreen;
            }

            _instance.ShowDialog();
        }

        private void InitializeData() {
            this.Controls.Clear();

            _panelBox = new Panel();

            _panelBox.Location = _indent;

            _panelBox.Width = this.Width - _scrollWidth - _indent.X;
            _panelBox.Height =
                (SettingsSystem.Instance.wallpapers.Count / _previewsInLine +
                (SettingsSystem.Instance.wallpapers.Count % _previewsInLine != 0 ? 1 : 0)) *
                (_previewHeight + _previewsVerticalSpace);

            _panelBox.Anchor = (
                AnchorStyles.Top |
                AnchorStyles.Left |
                AnchorStyles.Right
            );

            _panelBox.LocationChanged += PanelBox_LocationChanged;

            this.Controls.Add(_panelBox);

            InterfaceUpdate();
        }

        private void InterfaceUpdate() {
            Point imageViewPosition = new Point(0, 0);
            _currentPreviewsInLine = 0;
            _previewsInLine = _panelBox.Width / (_previewWidth + _previewsHorizontalSpace);
            _currentPreviewsHorizontalSpace = (_panelBox.Width - _previewWidth * _previewsInLine - _indent.X * 2) / (_previewsInLine - 1);

            if (_panelBox.Controls.Count != 0) {
                foreach (PictureBox box in _panelBox.Controls) {
                    box.Location = imageViewPosition;

                    imageViewPosition.X += box.Width + _currentPreviewsHorizontalSpace;
                    _currentPreviewsInLine++;

                    if (_currentPreviewsInLine >= _previewsInLine) {
                        imageViewPosition.X = 0;
                        imageViewPosition.Y += box.Height + 10;
                        _currentPreviewsInLine = 0;
                    }
                }
            }
            else {
                foreach (string img in SettingsSystem.Instance.wallpapers) {
                    //Thread t = new Thread(() => asyncResizePreview(img, imageViewPosition));
                    //t.Start();
                    try {
                        if (!_previews.ContainsKey(img)) {
                            Image imageFromFile = Image.FromFile(img);
                            Image previewImage = ImageController.resizedImage(imageFromFile, Math.Max(_previewWidth, _previewHeight));
                            _previews[img] = previewImage;
                        }
                    }
                    catch {
                        Console.WriteLine("Couldn't find image: " + img);
                        continue;
                    }
                    PictureBox previewBox = new PictureBox();

                    previewBox.Image = _previews[img];
                    previewBox.InitialImage = previewBox.Image;
                    previewBox.Width = _previewWidth;
                    previewBox.Height = _previewHeight;
                    previewBox.SizeMode = PictureBoxSizeMode.Zoom;
                    previewBox.Location = imageViewPosition;
                    previewBox.AccessibleDescription = img;

                    imageViewPosition.X += previewBox.Width + _currentPreviewsHorizontalSpace;
                    _currentPreviewsInLine++;

                    if (_currentPreviewsInLine >= _previewsInLine) {
                        imageViewPosition.X = 0;
                        imageViewPosition.Y += previewBox.Height + 10;
                        _currentPreviewsInLine = 0;
                    }

                    previewBox.MouseDoubleClick += PictureBox_DoubleClick;
                    previewBox.ContextMenuStrip = picBoxCMenu;

                    _panelBox.Controls.Add(previewBox);
                }
            }

            _panelBox.Height =
                (SettingsSystem.Instance.wallpapers.Count / _previewsInLine +
                (SettingsSystem.Instance.wallpapers.Count % _previewsInLine != 0 ? 1 : 0)) *
                (_previewHeight + _previewsVerticalSpace);
        }

        //private void asyncResizePreview(string img, Point imageViewPosition, Thread thread) {
        //    try {
        //        if (!_previews.ContainsKey(img)) {
        //            Image imageFromFile = Image.FromFile(img);
        //            Image previewImage = ImageController.resizedImage(imageFromFile, Math.Max(_previewWidth, _previewHeight));
        //            _previews[img] = previewImage;
        //        }
        //    }
        //    catch {
        //        Console.WriteLine("Couldn't find image: " + img);
        //        return;
        //    }
        //    PictureBox previewBox = new PictureBox();

        //    previewBox.Image = _previews[img];
        //    previewBox.InitialImage = previewBox.Image;
        //    previewBox.Width = _previewWidth;
        //    previewBox.Height = _previewHeight;
        //    previewBox.SizeMode = PictureBoxSizeMode.Zoom;
        //    previewBox.Location = imageViewPosition;
        //    previewBox.AccessibleDescription = img;

        //    imageViewPosition.X += previewBox.Width + _currentPreviewsHorizontalSpace;
        //    _currentPreviewsInLine++;

        //    if (_currentPreviewsInLine >= _previewsInLine) {
        //        imageViewPosition.X = 0;
        //        imageViewPosition.Y += previewBox.Height + 10;
        //        _currentPreviewsInLine = 0;
        //    }

        //    previewBox.MouseDoubleClick += PictureBox_DoubleClick;
        //    previewBox.ContextMenuStrip = picBoxCMenu;

        //    _panelBox.Controls.Add(previewBox);
        //}

        /// <summary>
        /// Показать файл по указанному пути в проводнике.
        /// </summary>
        /// <param name="path">Путь до файла.</param>
        /// <param name="select">Выделить указанный файл.</param>
        public static void openFolder(string path, bool select = false) {
            if (select) {
                Process.Start("explorer.exe", @"/select, " + @path);
            }
            else {
                Process.Start("explorer.exe", @path);
            }
        }

        #endregion

        #region Events

        private void GalleryForm_Resize(object sender, EventArgs e) {
            InterfaceUpdate();
        }

        private void PictureBox_DoubleClick(object sender, MouseEventArgs e) {
            PictureBox senderBox = sender as PictureBox;
            openFolder(senderBox.AccessibleDescription, true);
        }

        private void показатьВПапкеToolStripMenuItem_Click(object sender, EventArgs e) {
            ToolStripMenuItem senderItem = sender as ToolStripMenuItem;
            ContextMenuStrip contextMenuStrip = senderItem.Owner as ContextMenuStrip;
            Control senderControl = contextMenuStrip.SourceControl;
            openFolder(senderControl.AccessibleDescription, true);
        }

        private void установитьToolStripMenuItem_Click(object sender, EventArgs e) {
            if (_parent != null) {
                ToolStripMenuItem senderItem = sender as ToolStripMenuItem;
                ContextMenuStrip contextMenuStrip = senderItem.Owner as ContextMenuStrip;
                Control senderControl = contextMenuStrip.SourceControl;

                mainWnd parentForm = _parent as mainWnd;
                SettingsSystem.Instance.selectedWallpaper =
                    SettingsSystem.Instance.wallpapers.IndexOf(senderControl.AccessibleDescription);
                parentForm.fBtnChangeImgClick();
                parentForm.fBtnSelectedImgClick();
            }
        }

        private void PanelBox_LocationChanged(object sender, EventArgs e) {
            if (picBoxCMenu.Visible) {
                picBoxCMenu.Visible = false;
            }
        }

        #endregion
    }
}
