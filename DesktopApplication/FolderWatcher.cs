﻿using System.IO;
using System.Diagnostics;

// https://msdn.microsoft.com/ru-ru/library/system.io.filesystemwatcher(v=vs.110).aspx
// https://msdn.microsoft.com/ru-ru/library/ch2s8yd7(v=vs.90).aspx
// https://msdn.microsoft.com/ru-ru/library/system.io.filesystemwatcher.notifyfilter(v=vs.110).aspx

namespace DesktopApplication {
    class FolderWatcher {
        #region Fields

        private static FolderWatcher _Instance;
        private FileSystemWatcher _FolderWatcher;

        #endregion

        #region Methods

        public static void Initialize(string path) {
            if (_Instance == null) {
                _Instance = new FolderWatcher();
            }

            _Instance._FolderWatcher = new FileSystemWatcher(path);

            _Instance._FolderWatcher.Created += new FileSystemEventHandler(_Instance.FolderWatcher_Created);
            _Instance._FolderWatcher.Deleted += new FileSystemEventHandler(_Instance.FolderWatcher_Deleted);
            _Instance._FolderWatcher.Changed += new FileSystemEventHandler(_Instance.FolderWatcher_Changed);
            _Instance._FolderWatcher.Renamed += new RenamedEventHandler(_Instance.FolderWatcher_Renamed);

            _Instance._FolderWatcher.EnableRaisingEvents = true;
        }

        #endregion

        private void FolderWatcher_Created(object source, FileSystemEventArgs e) {
            Debug.Print("FW_Created");
        }

        private void FolderWatcher_Deleted(object source, FileSystemEventArgs e) {
            Debug.Print("FW_Deleted");
        }

        private void FolderWatcher_Changed(object source, FileSystemEventArgs e) {
            Debug.Print("FW_Changed");
        }

        private void FolderWatcher_Renamed(object source, RenamedEventArgs e) {
            Debug.Print("FW_Renamed");
        }
    }
}
