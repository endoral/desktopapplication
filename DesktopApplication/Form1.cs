﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace DesktopApplication {
    public partial class mainWnd: Form {
        #region Constructors

        public mainWnd() {
            InitializeComponent();
        }

        #endregion

        #region Variables

        /// <summary>
        /// Расстояние срабатывания приклеивания окна к краям экрана.
        /// </summary>
        const int DISTANCE = 10;

        public static mainWnd Instance;

        #endregion

        #region Methods

        /// <summary>
        /// Дополнительный функционал для приклеивания окна к краям экрана.
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m) {
            if (SettingsSystem.Instance.checkStickyBoundsState) {
                if (m.Msg == 0x0046 /* WM_WINDOWPOSCHANGING */) {
                    Rectangle workArea = SystemInformation.WorkingArea;
                    Rectangle rect = (Rectangle)Marshal.PtrToStructure(
                        (IntPtr)(IntPtr.Size * 2 + m.LParam.ToInt64()),
                        typeof(Rectangle));

                    if (rect.X <= workArea.Left + DISTANCE)
                        Marshal.WriteInt32(m.LParam, IntPtr.Size * 2, workArea.Left);

                    if (rect.X + rect.Width >= workArea.Width - DISTANCE)
                        Marshal.WriteInt32(m.LParam, IntPtr.Size * 2, workArea.Right - rect.Width);

                    if (rect.Y <= workArea.Top + DISTANCE)
                        Marshal.WriteInt32(m.LParam, IntPtr.Size * 2 + 4, workArea.Top);

                    if (rect.Y + rect.Height >= workArea.Height - DISTANCE)
                        Marshal.WriteInt32(m.LParam, IntPtr.Size * 2 + 4, workArea.Bottom - rect.Height);
                }
            }

            base.WndProc(ref m);
        }

        /// <summary>
        /// Обновление списка изображений.
        /// </summary>
        private void fRefreshWallpapersList() {
            SettingsSystem.Instance.previousWallpapers.Clear();

            try {
                LogSystem.Log("Очистка списка выбранных изображений...");
                SettingsSystem.Instance.wallpapers.Clear();
            }
            catch (SystemException ex) {
                LogSystem.Log(ex.Message);
            }

            LogSystem.Log("Выбранная папка: " + dlgOpenFolder.SelectedPath + ";");
            SettingsSystem.Instance.folderDialogPath = dlgOpenFolder.SelectedPath;
            LogSystem.Log("Поле настроек: " + SettingsSystem.Instance.folderDialogPath);
            LogSystem.Log("Включая вложенные папки: " + SettingsSystem.Instance.searchInSubfoldersState);

            try {
                foreach (string wallpaperName in Directory.GetFiles(
                    dlgOpenFolder.SelectedPath,
                    "*.jpg",
                    SettingsSystem.Instance.searchInSubfoldersState ?
                        SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)) {
                    if (wallpaperName != null) {
                        SettingsSystem.Instance.wallpapers.Add(wallpaperName);
                    }
                }
            }
            catch (System.Exception ex) {
                LogSystem.Log(ex.Message);
            }

            LogSystem.Log("Изображений в выбранной папке найдено: " + SettingsSystem.Instance.wallpapers.Count);

            if (SettingsSystem.Instance.wallpapers.Count > 0) {
                boxWallpaper.ImageLocation =
                        SettingsSystem.Instance.wallpapers[
                        SettingsSystem.Instance.selectedWallpaper];

                lbFileNumber.Text =
                    SettingsSystem.Instance.selectedWallpaper + 1 +
                    " / " +
                    SettingsSystem.Instance.wallpapers.Count;

                lbFileName.Text =
                    GetFileName(SettingsSystem.Instance.wallpapers[
                    SettingsSystem.Instance.selectedWallpaper]);

                lbFolderPath.Text =
                    SettingsSystem.Instance.folderDialogPath;

                SettingsSystem.Instance.btnFirstImageEnabled = true;
                SettingsSystem.Instance.btnPrevImageEnabled = true;
                SettingsSystem.Instance.btnRndImageEnabled = true;
                SettingsSystem.Instance.btnNextImageEnabled = true;
                SettingsSystem.Instance.btnLastImageEnabled = true;
                SettingsSystem.Instance.btnSelectedImageEnabled = true;
                SettingsSystem.Instance.btnChangeImageEnabled = true;
                SettingsSystem.Instance.checkStartWithRandomImageEnabled = true;
                SettingsSystem.Instance.checkAutochangeImageEnabled = true;

                btnFirstImg.Enabled = true;
                btnPrevImg.Enabled = true;
                btnRndImg.Enabled = true;
                btnNextImg.Enabled = true;
                btnLastImg.Enabled = true;
                btnSelectedImg.Enabled = true;
                btnChangeImg.Enabled = true;

                SettingsSystem.Instance.checkStartWithRandomImageEnabled = true;
                SettingsSystem.Instance.checkAutochangeImageEnabled = true;
            }
            else {
                boxWallpaper.ImageLocation = null;
                boxWallpaper.Image = null;
                boxWallpaper.Invalidate();

                lbFileNumber.Text = "- / -";
                lbFileName.Text = "-";
                lbFolderPath.Text = "-";
                dlgOpenFolder.SelectedPath = null;

                SettingsSystem.Instance.wallpapers.Clear();
                SettingsSystem.Instance.selectedWallpaper = 0;
                SettingsSystem.Instance.currentWallpaperIndex = 0;

                btnFirstImg.Enabled = false;
                btnPrevImg.Enabled = false;
                btnRndImg.Enabled = false;
                btnNextImg.Enabled = false;
                btnLastImg.Enabled = false;
                btnSelectedImg.Enabled = false;
                btnChangeImg.Enabled = false;

                SettingsSystem.Instance.checkStartWithRandomImageEnabled = false;
                SettingsSystem.Instance.checkAutochangeImageEnabled = false;
            }
        }

        private void fBtnFirstImgClick() {
            try {
                LogSystem.Log("Предпросмотр первого изображения из загруженных...");

                SettingsSystem.Instance.selectedWallpaper = 0;

                boxWallpaper.ImageLocation =
                    SettingsSystem.Instance.wallpapers[
                    SettingsSystem.Instance.selectedWallpaper];

                lbFileNumber.Text =
                    SettingsSystem.Instance.selectedWallpaper + 1 +
                    " / " +
                    SettingsSystem.Instance.wallpapers.Count;

                lbFileName.Text =
                    GetFileName(SettingsSystem.Instance.wallpapers[
                    SettingsSystem.Instance.selectedWallpaper]);
            }
            catch (SystemException ex) {
                LogSystem.Log(ex.Message);

                fRefreshWallpapersList();

                MessageBox.Show(
                    ex.Message,
                    "Ошибка!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void fBtnPrevImgClick() {
            if (SettingsSystem.Instance.selectedWallpaper != 0) {
                try {
                    LogSystem.Log("Предпросмотр предыдущего изображения из загруженных...");

                    boxWallpaper.ImageLocation =
                        SettingsSystem.Instance.wallpapers[
                        --SettingsSystem.Instance.selectedWallpaper];

                    lbFileNumber.Text =
                        SettingsSystem.Instance.selectedWallpaper + 1 +
                        " / " +
                        SettingsSystem.Instance.wallpapers.Count;

                    lbFileName.Text =
                        GetFileName(SettingsSystem.Instance.wallpapers[
                        SettingsSystem.Instance.selectedWallpaper]);
                }
                catch (SystemException ex) {
                    LogSystem.Log(ex.Message);

                    fRefreshWallpapersList();

                    MessageBox.Show(
                        ex.Message,
                        "Ошибка!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                    );
                }
            }
            else {
                fBtnLastImgClick();
            }
        }

        private void fBtnRndPrevImgClick() {
            if (SettingsSystem.Instance.previousWallpapers.Count > 0) {
                try {
                    LogSystem.Log("Предпросмотр предыдущего случайного изображения из загруженных...");

                    SettingsSystem.Instance.previousWallpapers.RemoveAt(0);
                    boxWallpaper.ImageLocation =
                                SettingsSystem.Instance.wallpapers[
                                SettingsSystem.Instance.previousWallpapers[0]];
                    SettingsSystem.Instance.selectedWallpaper = SettingsSystem.Instance.previousWallpapers[0];

                    lbFileNumber.Text =
                        SettingsSystem.Instance.selectedWallpaper + 1 +
                        " / " +
                        SettingsSystem.Instance.wallpapers.Count;

                    lbFileName.Text =
                        GetFileName(SettingsSystem.Instance.wallpapers[
                        SettingsSystem.Instance.previousWallpapers[0]]);
                }
                catch (SystemException ex) {
                    LogSystem.Log(ex.Message);

                    fRefreshWallpapersList();

                    MessageBox.Show(
                        ex.Message,
                        "Ошибка!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
        }

        private void fBtnRndImgClick() {
            Random uRnd = new Random();

            SettingsSystem.Instance.previousWallpapers.Insert(
                0, SettingsSystem.Instance.selectedWallpaper);

            LogSystem.Log("Вход в цикл...");

            if (SettingsSystem.Instance.wallpapers.Count <= 7) {
                do {
                    SettingsSystem.Instance.selectedWallpaper =
                        uRnd.Next(SettingsSystem.Instance.wallpapers.Count - 1);
                }
                while (CompareIndex((SettingsSystem.Instance.wallpapers.Count <= 3 ? 1 : 2)));
            }
            else {
                do {
                    SettingsSystem.Instance.selectedWallpaper =
                        uRnd.Next(SettingsSystem.Instance.wallpapers.Count - 1);
                }
                while (CompareIndex(5));
            }
            LogSystem.Log("Выход из цикла.");

            try {
                LogSystem.Log("Предпросмотр случайного изображения из загруженных...");

                boxWallpaper.ImageLocation =
                    SettingsSystem.Instance.wallpapers[
                    SettingsSystem.Instance.selectedWallpaper];

                lbFileNumber.Text =
                    SettingsSystem.Instance.selectedWallpaper + 1 +
                    " / " +
                    SettingsSystem.Instance.wallpapers.Count;

                lbFileName.Text =
                    GetFileName(SettingsSystem.Instance.wallpapers[
                    SettingsSystem.Instance.selectedWallpaper]);
            }
            catch (SystemException ex) {
                LogSystem.Log(ex.Message);

                fRefreshWallpapersList();

                MessageBox.Show(
                        ex.Message,
                        "Ошибка!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                    );
            }
        }

        // - Процедура выбора следующего изображения из списка
        private void fBtnNextImgClick() {
            if (SettingsSystem.Instance.selectedWallpaper !=
                SettingsSystem.Instance.wallpapers.Count - 1) {
                try {
                    LogSystem.Log("Предпросмотр следующего изображения из загруженных...");
                    SettingsSystem.Instance.previousWallpapers.Insert(
                        0,
                        SettingsSystem.Instance.selectedWallpaper);

                    boxWallpaper.ImageLocation =
                        SettingsSystem.Instance.wallpapers[
                        ++SettingsSystem.Instance.selectedWallpaper];

                    lbFileNumber.Text =
                        SettingsSystem.Instance.selectedWallpaper + 1 +
                        " / " +
                        SettingsSystem.Instance.wallpapers.Count;

                    lbFileName.Text =
                        GetFileName(SettingsSystem.Instance.wallpapers[
                        SettingsSystem.Instance.selectedWallpaper]);
                }
                catch (SystemException ex) {
                    LogSystem.Log(ex.Message);

                    fRefreshWallpapersList();

                    MessageBox.Show(
                        ex.Message,
                        "Ошибка!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
            else {
                fBtnFirstImgClick();
            }
        }

        private void fBtnLastImgClick() {
            try {
                LogSystem.Log("Предпросмотр последнего изображения из загруженных...");

                SettingsSystem.Instance.selectedWallpaper =
                    SettingsSystem.Instance.wallpapers.Count - 1;

                boxWallpaper.ImageLocation =
                    SettingsSystem.Instance.wallpapers[
                    SettingsSystem.Instance.selectedWallpaper];

                lbFileNumber.Text =
                    SettingsSystem.Instance.selectedWallpaper + 1 +
                    " / " +
                    SettingsSystem.Instance.wallpapers.Count;

                lbFileName.Text =
                    GetFileName(SettingsSystem.Instance.wallpapers[
                    SettingsSystem.Instance.selectedWallpaper]);
            }
            catch (SystemException ex) {
                LogSystem.Log(ex.Message);

                fRefreshWallpapersList();

                MessageBox.Show(
                        ex.Message,
                        "Ошибка!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                    );
            }
        }

        public void fBtnSelectedImgClick() {
            try {
                LogSystem.Log("Предпросмотр установленного в качестве обоев изображения из загруженных...");

                SettingsSystem.Instance.selectedWallpaper =
                    SettingsSystem.Instance.currentWallpaperIndex;

                boxWallpaper.ImageLocation =
                    SettingsSystem.Instance.wallpapers[
                    SettingsSystem.Instance.selectedWallpaper];

                lbFileNumber.Text =
                    SettingsSystem.Instance.selectedWallpaper + 1 +
                    " / " +
                    SettingsSystem.Instance.wallpapers.Count;

                lbFileName.Text =
                    GetFileName(SettingsSystem.Instance.wallpapers[
                    SettingsSystem.Instance.selectedWallpaper]);
            }
            catch (SystemException ex) {
                LogSystem.Log(ex.Message);

                fRefreshWallpapersList();

                MessageBox.Show(
                        ex.Message,
                        "Ошибка!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
            }
        }

        public void fBtnChangeImgClick() {
            try {
                LogSystem.Log("Установка новых обоев...");

                LogSystem.Log(
                    "Текущие обои: " +
                    SettingsSystem.Instance.wallpapers[
                    SettingsSystem.Instance.currentWallpaperIndex]);

                LogSystem.Log(
                    "Устанавливаемые обои: " +
                    SettingsSystem.Instance.wallpapers[
                    SettingsSystem.Instance.selectedWallpaper]);

                SettingsSystem.Instance.currentWallpaperIndex = SettingsSystem.Instance.selectedWallpaper;
                boxWallpaper.ImageLocation =
                    SettingsSystem.Instance.wallpapers[SettingsSystem.Instance.selectedWallpaper];

                SystemParametersInfo(20, 0, boxWallpaper.ImageLocation, 0x01 | 0x02);

                LogSystem.Log("Визуализация установленного изображения в предпросмотре...");
                boxWallpaper.Invalidate();
            }
            catch (SystemException ex) {
                LogSystem.Log(ex.Message);
            }
        }

        #endregion

        #region Events

        // - События при загрузке главного окна
        private void MainWnd_Load(object sender, EventArgs e) {
            LogSystem.SetApplicationPath(Application.StartupPath);

            trayContMenuBtnExit.ToolTipText = LogSystem.LogPath;

            LogSystem.Log("Приложение успешно запущено из: " + Application.ExecutablePath);

            SettingsSystem.isInitialized = true;

            try {
                LogSystem.Log("Отображение текущей версии приложения...");
                lbProgVer.Text = "Version: " + Application.ProductVersion;
                LogSystem.Log(lbProgVer.Text + ";");

                if (System.Diagnostics.Debugger.IsAttached) {
                    trayIcon.Text += " (debug)";
                }
            }
            catch (SystemException ex) {
                LogSystem.Log(ex.Message);
            }

            if (mainWnd.Instance == null) {
                mainWnd.Instance = this;
            }

            LoadSettings();

            if (File.Exists(SettingsSystem.settingsReserveFileName)) {
                LogSystem.Log("Удаление резервной копии предыдущих параметров настройки");
                File.Delete(SettingsSystem.settingsReserveFileName);
            }

            SettingsSystem.isInitialized = false;
        }

        private void LoadSettings() {
            if (File.Exists(SettingsSystem.settingsFileName)) {
                try {
                    LogSystem.Log("Создание файлового потока для десериализации и загрузки настроек...");
                    using (Stream uStream =
                        new FileStream(SettingsSystem.settingsFileName, FileMode.Open)) {
                        LogSystem.Log("Инициализация десериализации...");
                        XmlSerializer uSerializer =
                            new XmlSerializer(typeof(SettingsSystem));

                        LogSystem.Log("Десериализация настроек...");
                        SettingsSystem.SetInstance((SettingsSystem)uSerializer.Deserialize(uStream));

                        LogSystem.Log("Уничтожения файлового потока...");
                        uStream.Close();
                        uStream.Dispose();

                        SettingsForm.Autorun(SettingsSystem.Instance.checkAutorunState);

                        trayIcon.Visible = SettingsSystem.Instance.checkAlwaysInTrayState;

                        if (SettingsSystem.Instance.checkAutochangeImageState) {
                            LogSystem.Log("Изменение статуса автосмены изображения.");

                            if (!imgAutoChangeThread.IsBusy) {
                                SettingsSystem.Instance.checkAutochangeImageState = true;
                                LogSystem.Log(
                                    "Поле настроек: " +
                                    SettingsSystem.Instance.checkAutochangeImageState + ".");

                                try {
                                    LogSystem.Log("Запуск работы фонового потока...");
                                    imgAutoChangeThread.RunWorkerAsync(null);
                                }
                                catch (SystemException ex) {
                                    LogSystem.Log(ex.Message);
                                }
                            }
                            else {
                                LogSystem.Log("Фоновый поток занят!");
                            }
                        }

                        // - Состояние кнопок вкладки "Навигация"
                        btnFirstImg.Enabled = SettingsSystem.Instance.btnFirstImageEnabled;
                        btnPrevImg.Enabled = SettingsSystem.Instance.btnPrevImageEnabled;
                        btnRndImg.Enabled = SettingsSystem.Instance.btnRndImageEnabled;
                        btnNextImg.Enabled = SettingsSystem.Instance.btnNextImageEnabled;
                        btnLastImg.Enabled = SettingsSystem.Instance.btnLastImageEnabled;
                        btnSelectedImg.Enabled = SettingsSystem.Instance.btnSelectedImageEnabled;
                        btnChangeImg.Enabled = SettingsSystem.Instance.btnChangeImageEnabled;

                        boxWallpaper.ImageLocation = SettingsSystem.Instance.imageBoxLocation;
                        LogSystem.Log("Установка текущей выбранной папки с изображениями...");
                        dlgOpenFolder.SelectedPath = SettingsSystem.Instance.folderDialogPath;
                        LogSystem.Log("Папка: " + dlgOpenFolder.SelectedPath);
                        LogSystem.Log("Поле: " + SettingsSystem.Instance.folderDialogPath);

                        LogSystem.Log("Установка размеров окна программы...");
                        this.Size = SettingsSystem.Instance.size;

                        LogSystem.Log("Установка положения окна программы на экране...");
                        this.Location = SettingsSystem.Instance.location;

                        LogSystem.Log(
                            "Координаты на экране: " +
                            this.Location.X + ", " + this.Location.Y);

                        LogSystem.Log(
                            "Поле: " +
                            SettingsSystem.Instance.location.X + ", " + SettingsSystem.Instance.location.Y);

                        LogSystem.Log("Установка статуса окна программы...");

                        this.WindowState = SettingsSystem.Instance.windowState;

                        LogSystem.Log("Статус: " + this.WindowState);
                        LogSystem.Log("Поле: " + SettingsSystem.Instance.windowState);

                        if (SettingsSystem.Instance.wallpapers.Count > 0) {
                            lbFileNumber.Text =
                                SettingsSystem.Instance.selectedWallpaper + 1 +
                                " / " +
                                SettingsSystem.Instance.wallpapers.Count;

                            lbFileName.Text =
                                GetFileName(SettingsSystem.Instance.wallpapers[
                                SettingsSystem.Instance.selectedWallpaper]);
                        }

                        lbFolderPath.Text = dlgOpenFolder.SelectedPath;

                        if (SettingsSystem.Instance.checkStartWithRandomImageState) {
                            fBtnRndImgClick();
                            fBtnChangeImgClick();
                        }
                        else {
                            fBtnChangeImgClick();
                        }
                    }

                    LogSystem.Log("Настройки успешно загружены!");
                }
                catch (SystemException ex) {
                    LogSystem.Log("Ошибка при загрузке настроек!");
                    LogSystem.Log(ex.Message);

                    LogSystem.Log("Попытка восстановления параметров настройки из резервной копии...");
                    if (File.Exists(SettingsSystem.settingsReserveFileName)) {
                        File.Delete(SettingsSystem.settingsFileName);

                        LogSystem.Log("Копирование резервной копии предыдущих параметров настройки в текущие...");
                        File.Copy(
                            SettingsSystem.settingsReserveFileName,
                            SettingsSystem.settingsFileName, true);

                        LogSystem.Log("Удаление резервной копии предыдущих параметров настройки");
                        File.Delete(SettingsSystem.settingsReserveFileName);

                        LoadSettings();
                    }
                }
            }
            else {
                SettingsSystem.SetInstance(SettingsSystem.Instance);
                LogSystem.Log("Файл настроек не найден, запуск с параметрами по умолчанию.");
            }
        }

        // - События при закрытии главного окна
        private void MainWnd_FormClosing(object sender, FormClosingEventArgs e) {
            // - Приложение закрывается системой
            if (e.CloseReason != CloseReason.UserClosing) {
                e.Cancel = false;

                SettingsSystem.SaveData();

                try {
                    LogSystem.Log("Остановка фонового потока...");
                    imgAutoChangeThread.CancelAsync();
                    LogSystem.Log("Уничтожение фонового потока...");
                    imgAutoChangeThread.Dispose();
                }
                catch (SystemException ex) {
                    LogSystem.Log(ex.Message);
                }

                trayIcon.Visible = false;

                LogSystem.Log("Завершение работы приложения...");
                LogSystem.Log("Причина завершения: " + e.CloseReason.ToString() + ".");
                LogSystem.Log();
                LogSystem.deinit();
            }
            else {
                if (SettingsSystem.Instance.minimizeByX && this.WindowState != FormWindowState.Minimized) {
                    e.Cancel = true;

                    this.WindowState = FormWindowState.Minimized;

                    return;
                }

                trayContMenuBtnExit.Enabled = false;

                if (SettingsSystem.Instance.dontAskWhenCloseState ||
                    QuitAccept.ShowMessage() == DialogResult.Yes) {
                    LogSystem.Log("Завершение работы приложения с согласия пользователя...");

                    e.Cancel = false;

                    SettingsSystem.SaveData();

                    try {
                        LogSystem.Log("Остановка фонового потока...");
                        imgAutoChangeThread.CancelAsync();
                        LogSystem.Log("Уничтожение фонового потока...");
                        imgAutoChangeThread.Dispose();
                    }
                    catch (SystemException ex) {
                        LogSystem.Log(ex.Message);
                    }

                    trayIcon.Visible = false;

                    LogSystem.Log("Завершение работы приложения...");
                    LogSystem.Log("Причина завершения: " + e.CloseReason.ToString() + ".");
                    LogSystem.Log();
                    LogSystem.deinit();
                }
                else {
                    e.Cancel = true;
                }

                trayContMenuBtnExit.Enabled = true;
            }
        }

        private void btnLoadFiles_Click(object sender, EventArgs e) {
            LogSystem.Log("Вызов клика по кнопке открытия папки с изображениями.");

            if (dlgOpenFolder.ShowDialog() == DialogResult.OK) {
                LogSystem.Log("Пользователем выбрана папка.");
                fRefreshWallpapersList();

                if (SettingsSystem.Instance.wallpapers.Count <= 0) {
                    LogSystem.Log("Изображения формата *.jpg отсутствуют в выбранной папке!");
                    MessageBox.Show
                        ("Изображения нужного формата отсутствуют в выбранной папке.",
                        "Внимание!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }
            }
        }

        private void btnSettings_Click(object sender, EventArgs e) {
            SettingsForm.ShowModal();
        }

        private void button1_Click(object sender, EventArgs e) {
            GalleryForm.ShowModal(this);
        }

        private void btnFirstImg_Click(object sender, EventArgs e) {
            LogSystem.Log("Вызов клика по кнопке \"" + btnFirstImg.Text + "\".");
            fBtnFirstImgClick();
        }

        private void btnPrevImg_Click(object sender, EventArgs e) {
            //LogSystem.Log("Вызов клика по кнопке \"" + btnPrevImg.Text + "\".");
            //fBtnPrevImgClick();
        }

        private void btnPrevImg_MouseUp(object sender, MouseEventArgs e) {
            LogSystem.Log(
                "Вызов клика по кнопке \"" +
                btnPrevImg.Text +
                "\" " + e.Button +
                " кнопкой мыши.");

            if (e.Button == MouseButtons.Right) {
                fBtnRndPrevImgClick();
            }
            else {
                fBtnPrevImgClick();
            }
        }

        private void btnRndImg_Click(object sender, EventArgs e) {
            LogSystem.Log("Вызов клика по кнопке \"" + btnRndImg.Text + "\".");
            fBtnRndImgClick();
        }

        private void btnNextImg_Click(object sender, EventArgs e) {
            LogSystem.Log("Вызов клика по кнопке \"" + btnNextImg.Text + "\".");
            fBtnNextImgClick();
        }

        private void btnLastImg_Click(object sender, EventArgs e) {
            LogSystem.Log("Вызов клика по кнопке \"" + btnLastImg.Text + "\".");
            fBtnLastImgClick();
        }

        private void btnSelectedImg_Click(object sender, EventArgs e) {
            LogSystem.Log("Вызов клика по кнопке \"" + btnSelectedImg.Text + "\".");
            fBtnSelectedImgClick();
        }

        private void btnChangeImg_Click(object sender, EventArgs e) {
            LogSystem.Log("Вызов клика по кнопке \"" + btnChangeImg.Text + "\".");
            fBtnChangeImgClick();
        }

        private void mainWnd_Resize(object sender, EventArgs e) {
            LogSystem.Log("Изменение статуса окна приложения.");

            switch (this.WindowState) {
            case FormWindowState.Minimized:

                try {
                    LogSystem.Log("Статус: " + this.WindowState + ".");

                    if (SettingsSystem.Instance.wallpapers.Count > 0)
                        boxWallpaper.ImageLocation =
                            SettingsSystem.Instance.wallpapers[
                            SettingsSystem.Instance.currentWallpaperIndex];

                    boxWallpaper.Invalidate();

                    if (SettingsSystem.Instance.checkMinimizeToTrayState) {
                        this.ShowInTaskbar = false;
                        trayIcon.Visible = true;
                        HideFromAltTab(this.Handle);
                    }

                    trayContMenuBtnToNormal.Visible = true;
                    trayContMenuBtnToMinimize.Visible = false;
                }
                catch (SystemException ex) {
                    LogSystem.Log(ex.Message);
                }

                break;

            case FormWindowState.Normal:

                try {
                    LogSystem.Log("Статус: " + this.WindowState + ".");

                    this.ShowInTaskbar = true;

                    if (!SettingsSystem.Instance.checkAlwaysInTrayState) {
                        trayIcon.Visible = false;
                    }

                    trayContMenuBtnToMinimize.Visible = true;
                    trayContMenuBtnToNormal.Visible = false;
                }
                catch (SystemException ex) {
                    LogSystem.Log(ex.Message);
                }

                break;

            default:

                break;
            }
        }

        private void trayIcon_MouseDoubleClick(object sender, MouseEventArgs e) {
            LogSystem.Log(
                "Двойной клик мышью по иконке в трее." +
                "Кнопка: " + e.Button + ".");

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                trayContMenuBtnToNormal.PerformClick();
        }

        private void trayIcon_MouseClick(object sender, MouseEventArgs e) {
            LogSystem.Log(
                "Одиночный клик мышью по иконке в трее." +
                "Кнопка: " + e.Button + ".");

            if (e.Button == System.Windows.Forms.MouseButtons.Middle) {
                fBtnRndImgClick();
                fBtnChangeImgClick();
                SettingsSystem.SaveData();
            }
        }

        private void trayContMenuBtnNext_Click(object sender, EventArgs e) {
            LogSystem.Log(
                "Вызов клика по кнопке \"" +
                trayContMenuBtnNext.Text + "\" в трее.");

            fBtnNextImgClick();
            fBtnChangeImgClick();
            SettingsSystem.SaveData();
        }

        private void trayContMenuBtnPrev_Click(object sender, EventArgs e) {
            LogSystem.Log(
                "Вызов клика по кнопке \"" +
                trayContMenuBtnPrev.Text + "\" в трее.");

            fBtnPrevImgClick();
            fBtnChangeImgClick();
            SettingsSystem.SaveData();
        }

        private void trayContMenuBtnRnd_Click(object sender, EventArgs e) {
            LogSystem.Log(
                "Вызов клика по кнопке \"" +
                trayContMenuBtnRnd.Text + "\" в трее.");

            fBtnRndImgClick();
            fBtnChangeImgClick();
            SettingsSystem.SaveData();
        }

        private void trayConMenuBtnNormalState_Click(object sender, EventArgs e) {
            LogSystem.Log(
                "Вызов клика по кнопке \"" +
                trayContMenuBtnToNormal.Text + "\" в трее.");
            this.WindowState = FormWindowState.Normal;
        }

        private void trayContMenuBtnToMinimize_Click(object sender, EventArgs e) {
            LogSystem.Log(
                "Вызов клика по кнопке \"" +
                trayContMenuBtnToMinimize.Text + "\" в трее.");
            this.WindowState = FormWindowState.Minimized;
        }

        private void trayContMenuBtnExit_Click(object sender, EventArgs e) {
            LogSystem.Log(
                "Вызов клика по кнопке \"" +
                trayContMenuBtnExit.Text + "\" в трее.");
            this.Close();
        }

        private void boxWallpaper_DoubleClick(object sender, EventArgs e) {
            openFolder(SettingsSystem.Instance.wallpapers[SettingsSystem.Instance.selectedWallpaper], true);
        }

        private void lbMarkFolderPath_DoubleClick(object sender, EventArgs e) {
            openFolder(dlgOpenFolder.SelectedPath);
        }

        private void lbFolderPath_DoubleClick(object sender, EventArgs e) {
            openFolder(dlgOpenFolder.SelectedPath);
        }

        private void lbMarkFileName_DoubleClick(object sender, EventArgs e) {
            openFolder(SettingsSystem.Instance.wallpapers[SettingsSystem.Instance.selectedWallpaper], true);
        }

        private void lbFileName_DoubleClick(object sender, EventArgs e) {
            openFolder(SettingsSystem.Instance.wallpapers[SettingsSystem.Instance.selectedWallpaper], true);
        }

        private void lbMarkFileNumber_DoubleClick(object sender, EventArgs e) {
            openFolder(SettingsSystem.Instance.wallpapers[SettingsSystem.Instance.selectedWallpaper], true);
        }

        private void lbFileNumber_DoubleClick(object sender, EventArgs e) {
            openFolder(SettingsSystem.Instance.wallpapers[SettingsSystem.Instance.selectedWallpaper], true);
        }

        // Делегат для выполнения метода из другого процесса
        delegate void ClickControl(MethodInvoker uMethod);

        // Выполнение указанного метода из другого процесса
        void setClick(MethodInvoker method) {
            try {
                LogSystem.Log("Вызов метода \"" +
                    method.Method.Name +
                    "\" из фонового потока...");

                if (this.InvokeRequired) {
                    LogSystem.Log("Делегированный вызов.");
                    ClickControl cc =
                        new ClickControl(setClick);

                    this.Invoke(cc, method);
                }
                else {
                    LogSystem.Log("Обыкновенный вызов.");
                    method();
                }
            }
            catch (SystemException ex) {
                LogSystem.Log(ex.Message);
            }
        }

        // - События, выполняющиеся в отдельном потоке
        private void imgAutoChangeThread_DoWork(object sender, DoWorkEventArgs e) {
            while (SettingsSystem.Instance.checkAutochangeImageState) {
                LogSystem.Log(
                    "Фоновый поток уходит в ожидание на " +
                    SettingsSystem.Instance.numIntValue / 60000 + " минут.");

                Thread.Sleep((int)SettingsSystem.Instance.numIntValue);

                if (SettingsSystem.Instance.checkAutochangeImageState) {
                    LogSystem.Log("Начало автосмены изображения...");

                    LogSystem.Log(" Интервал: " + SettingsSystem.Instance.numIntValue / 60000 + " минут;");

                    if (SettingsSystem.Instance.checkRandomImageState) {
                        setClick(fBtnRndImgClick);
                    }
                    else {
                        setClick(fBtnNextImgClick);
                    }

                    setClick(fBtnChangeImgClick);

                    LogSystem.Log("Автосмена изображения прошла успешно!");
                }
            }
        }

        #endregion

        bool mouseDownFlag = false;
        bool dragFlag = false;
        Point dragLocation;
        GalleryForm galleryFakeBox;

        private void boxWallpaper_MouseDown(object sender, MouseEventArgs e) {
            if (!mouseDownFlag) {
                dragFlag = false;
                mouseDownFlag = true;
                dragLocation = e.Location;
            }
        }

        private void boxWallpaper_MouseUp(object sender, MouseEventArgs e) {
            if (mouseDownFlag) {
                mouseDownFlag = false;

                if (dragFlag) {
                    dragFlag = false;

                    if (galleryFakeBox != null) {
                        galleryFakeBox.Close();
                        galleryFakeBox.Dispose();
                        galleryFakeBox = null;
                    }

                    if (!boxWallpaper.DisplayRectangle.Contains(e.Location)) {
                        Point screenLocation = boxWallpaper.PointToScreen(e.Location);
                        Point fakeBoxLocation = new Point(
                            screenLocation.X - dragLocation.X,
                            screenLocation.Y - dragLocation.Y
                        );
                        GalleryForm.ShowModal(this, fakeBoxLocation.X, fakeBoxLocation.Y);
                    }
                }
            }
        }

        private void boxWallpaper_MouseMove(object sender, MouseEventArgs e) {
            if (mouseDownFlag) {
                if (!dragFlag) {
                    if (Math.Abs(dragLocation.X - e.Location.X) > 5 ||
                        Math.Abs(dragLocation.Y - e.Location.Y) > 5) {
                        dragFlag = true;
                        // - Создать рамку
                        galleryFakeBox = new GalleryForm();
                        galleryFakeBox.Location = boxWallpaper.PointToScreen(e.Location);
                        galleryFakeBox.Enabled = false;
                        galleryFakeBox.Show();
                    }
                }
                else {
                    // - Таскать рамку
                    if (galleryFakeBox != null) {
                        Point screenLocation = boxWallpaper.PointToScreen(e.Location);
                        Point fakeBoxLocation = new Point(
                            screenLocation.X - dragLocation.X,
                            screenLocation.Y - dragLocation.Y
                        );

                        galleryFakeBox.Location = fakeBoxLocation;
                    }
                }
            }
        }
    }
}