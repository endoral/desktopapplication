﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace DesktopApplication {
    public partial class mainWnd : Form {
        #region Variables

        // - Системные константы для сокрытия программы от списка ALT+TAB
        private const int GWL_EXSTYLE = -20;
        private const int WS_EX_TOOLWINDOW = 0x00000080;

        #endregion

        #region Methods

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern int SystemParametersInfo(int uAction, int uParam, string lpvParam, int fuWinIni);

        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr window, int index, int value);

        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr window, int index);

        /// <summary>
        /// Cокрытие программы от списка ALT+TAB.
        /// </summary>
        /// <param name="Handle">Указатель на окно.</param>
        public static void HideFromAltTab(IntPtr Handle) {
            SetWindowLong(
                Handle,
                GWL_EXSTYLE,
                GetWindowLong(Handle, GWL_EXSTYLE) | WS_EX_TOOLWINDOW
            );
        }

        /// <summary>
        /// Имя файла по полному пути к нему.
        /// </summary>
        /// <param name="uPathToFile">Полный путь к файлу.</param>
        /// <returns>Имя файла.</returns>
        private string GetFileName(string uPathToFile) {
            string uFileName = uPathToFile.Remove(
                0,
                uPathToFile.LastIndexOf(@"\") + 1
            );

            return uFileName;
        }

        private bool CompareIndex(int count) {
            try {
                LogSystem.Log("Процесс сравнения...");

                LogSystem.Log("Проверка на заполненность списка...");
                if (SettingsSystem.Instance.previousWallpapers.Count < 1) {
                    return false;
                }

                LogSystem.Log("Проверка на соответствие списка указанному размеру...");
                if (SettingsSystem.Instance.previousWallpapers.Count <= count) {
                    count = SettingsSystem.Instance.previousWallpapers.Count;
                }

                LogSystem.Log("Сравнение элементов...");
                LogSystem.Log(
                    "Заданная длинна: " +
                    count +
                    "; Фактическая длинна: " +
                    SettingsSystem.Instance.previousWallpapers.Count
                );

                for (int i = 0; i < count; i++) {
                    LogSystem.Log("Текущий элемет: " + i);
                    if (SettingsSystem.Instance.previousWallpapers[i] ==
                        SettingsSystem.Instance.selectedWallpaper) {
                        return true;
                    }
                }
            }
            catch (SystemException ex) {
                LogSystem.Log(ex.Message);
            }

            return false;
        }

        ///// <summary>
        ///// Показать файл по указанному пути в проводнике.
        ///// </summary>
        ///// <param name="path">Путь до файла.</param>
        //public static void openFolder(string path) {
        //    Process.Start("explorer.exe", @path);
        //}

        /// <summary>
        /// Показать файл по указанному пути в проводнике.
        /// </summary>
        /// <param name="path">Путь до файла.</param>
        /// <param name="select">Выделить указанный файл.</param>
        public static void openFolder(string path, bool select = false) {
            if (select) {
                Process.Start("explorer.exe", @"/select, " + @path);
            }
            else {
                Process.Start("explorer.exe", @path);
            }
        }

        #endregion
    }
}
