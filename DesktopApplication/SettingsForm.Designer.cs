﻿namespace DesktopApplication
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.autorunCh = new System.Windows.Forms.CheckBox();
            this.randomStartCh = new System.Windows.Forms.CheckBox();
            this.autochangeCh = new System.Windows.Forms.CheckBox();
            this.randomCh = new System.Windows.Forms.CheckBox();
            this.trayCh = new System.Windows.Forms.CheckBox();
            this.alwaysTrayCh = new System.Windows.Forms.CheckBox();
            this.intLb = new System.Windows.Forms.Label();
            this.intFormatLb = new System.Windows.Forms.Label();
            this.intNum = new System.Windows.Forms.NumericUpDown();
            this.dontAskWnenExitCh = new System.Windows.Forms.CheckBox();
            this.stickyBoundsCh = new System.Windows.Forms.CheckBox();
            this.searchInChildCh = new System.Windows.Forms.CheckBox();
            this.autoAddCh = new System.Windows.Forms.CheckBox();
            this.openInFolderBtn = new System.Windows.Forms.Button();
            this.autorunBox = new System.Windows.Forms.GroupBox();
            this.autochangeBox = new System.Windows.Forms.GroupBox();
            this.windowBox = new System.Windows.Forms.GroupBox();
            this.fileBox = new System.Windows.Forms.GroupBox();
            this.closeTypeBox = new System.Windows.Forms.GroupBox();
            this.minimizeByX = new System.Windows.Forms.RadioButton();
            this.closeByX = new System.Windows.Forms.RadioButton();
            this.buttonBox = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.intNum)).BeginInit();
            this.autorunBox.SuspendLayout();
            this.autochangeBox.SuspendLayout();
            this.windowBox.SuspendLayout();
            this.fileBox.SuspendLayout();
            this.closeTypeBox.SuspendLayout();
            this.buttonBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // autorunCh
            // 
            this.autorunCh.AutoSize = true;
            this.autorunCh.Location = new System.Drawing.Point(14, 19);
            this.autorunCh.Name = "autorunCh";
            this.autorunCh.Size = new System.Drawing.Size(85, 17);
            this.autorunCh.TabIndex = 0;
            this.autorunCh.Text = "Автозапуск";
            this.autorunCh.UseVisualStyleBackColor = true;
            this.autorunCh.CheckedChanged += new System.EventHandler(this.autorunCh_CheckedChanged);
            // 
            // randomStartCh
            // 
            this.randomStartCh.AutoSize = true;
            this.randomStartCh.Location = new System.Drawing.Point(14, 42);
            this.randomStartCh.Name = "randomStartCh";
            this.randomStartCh.Size = new System.Drawing.Size(208, 17);
            this.randomStartCh.TabIndex = 1;
            this.randomStartCh.Text = "Случайное изображение при старте";
            this.randomStartCh.UseVisualStyleBackColor = true;
            this.randomStartCh.CheckedChanged += new System.EventHandler(this.randomStartCh_CheckedChanged);
            // 
            // autochangeCh
            // 
            this.autochangeCh.AutoSize = true;
            this.autochangeCh.Location = new System.Drawing.Point(14, 19);
            this.autochangeCh.Name = "autochangeCh";
            this.autochangeCh.Size = new System.Drawing.Size(153, 17);
            this.autochangeCh.TabIndex = 2;
            this.autochangeCh.Text = "Автосмена изображения";
            this.autochangeCh.UseVisualStyleBackColor = true;
            this.autochangeCh.CheckedChanged += new System.EventHandler(this.autochangeCh_CheckedChanged);
            // 
            // randomCh
            // 
            this.randomCh.AutoSize = true;
            this.randomCh.Location = new System.Drawing.Point(14, 43);
            this.randomCh.Name = "randomCh";
            this.randomCh.Size = new System.Drawing.Size(150, 17);
            this.randomCh.TabIndex = 3;
            this.randomCh.Text = "Случайное изображение";
            this.randomCh.UseVisualStyleBackColor = true;
            this.randomCh.CheckedChanged += new System.EventHandler(this.randomCh_CheckedChanged);
            // 
            // trayCh
            // 
            this.trayCh.AutoSize = true;
            this.trayCh.Location = new System.Drawing.Point(14, 19);
            this.trayCh.Name = "trayCh";
            this.trayCh.Size = new System.Drawing.Size(126, 17);
            this.trayCh.TabIndex = 4;
            this.trayCh.Text = "Сворачивать в трей";
            this.trayCh.UseVisualStyleBackColor = true;
            this.trayCh.CheckedChanged += new System.EventHandler(this.trayCh_CheckedChanged);
            // 
            // alwaysTrayCh
            // 
            this.alwaysTrayCh.AutoSize = true;
            this.alwaysTrayCh.Location = new System.Drawing.Point(14, 43);
            this.alwaysTrayCh.Name = "alwaysTrayCh";
            this.alwaysTrayCh.Size = new System.Drawing.Size(97, 17);
            this.alwaysTrayCh.TabIndex = 5;
            this.alwaysTrayCh.Text = "Всегда в трее";
            this.alwaysTrayCh.UseVisualStyleBackColor = true;
            this.alwaysTrayCh.CheckedChanged += new System.EventHandler(this.alwaysTrayCh_CheckedChanged);
            // 
            // intLb
            // 
            this.intLb.AutoSize = true;
            this.intLb.Location = new System.Drawing.Point(14, 78);
            this.intLb.Name = "intLb";
            this.intLb.Size = new System.Drawing.Size(59, 13);
            this.intLb.TabIndex = 6;
            this.intLb.Text = "Интервал:";
            // 
            // intFormatLb
            // 
            this.intFormatLb.AutoSize = true;
            this.intFormatLb.Location = new System.Drawing.Point(128, 78);
            this.intFormatLb.Name = "intFormatLb";
            this.intFormatLb.Size = new System.Drawing.Size(37, 13);
            this.intFormatLb.TabIndex = 7;
            this.intFormatLb.Text = "минут";
            // 
            // intNum
            // 
            this.intNum.Location = new System.Drawing.Point(73, 76);
            this.intNum.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.intNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.intNum.Name = "intNum";
            this.intNum.Size = new System.Drawing.Size(49, 20);
            this.intNum.TabIndex = 8;
            this.intNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.intNum.ValueChanged += new System.EventHandler(this.intNum_ValueChanged);
            this.intNum.KeyUp += new System.Windows.Forms.KeyEventHandler(this.intNum_KeyUp);
            // 
            // dontAskWnenExitCh
            // 
            this.dontAskWnenExitCh.AutoSize = true;
            this.dontAskWnenExitCh.Location = new System.Drawing.Point(14, 66);
            this.dontAskWnenExitCh.Name = "dontAskWnenExitCh";
            this.dontAskWnenExitCh.Size = new System.Drawing.Size(177, 17);
            this.dontAskWnenExitCh.TabIndex = 9;
            this.dontAskWnenExitCh.Text = "Не спрашивать при закрытии";
            this.dontAskWnenExitCh.UseVisualStyleBackColor = true;
            this.dontAskWnenExitCh.CheckedChanged += new System.EventHandler(this.dontAskWnenExitCh_CheckedChanged);
            // 
            // stickyBoundsCh
            // 
            this.stickyBoundsCh.AutoSize = true;
            this.stickyBoundsCh.Location = new System.Drawing.Point(14, 89);
            this.stickyBoundsCh.Name = "stickyBoundsCh";
            this.stickyBoundsCh.Size = new System.Drawing.Size(176, 17);
            this.stickyBoundsCh.TabIndex = 10;
            this.stickyBoundsCh.Text = "Приклеивать к краям экрана";
            this.stickyBoundsCh.UseVisualStyleBackColor = true;
            this.stickyBoundsCh.CheckedChanged += new System.EventHandler(this.stickyBoundsCh_CheckedChanged);
            // 
            // searchInChildCh
            // 
            this.searchInChildCh.AutoSize = true;
            this.searchInChildCh.Location = new System.Drawing.Point(14, 19);
            this.searchInChildCh.Name = "searchInChildCh";
            this.searchInChildCh.Size = new System.Drawing.Size(154, 17);
            this.searchInChildCh.TabIndex = 11;
            this.searchInChildCh.Text = "Поиск в дочерних папках";
            this.searchInChildCh.UseVisualStyleBackColor = true;
            this.searchInChildCh.CheckedChanged += new System.EventHandler(this.searchInChildCh_CheckedChanged);
            // 
            // autoAddCh
            // 
            this.autoAddCh.AutoSize = true;
            this.autoAddCh.Enabled = false;
            this.autoAddCh.Location = new System.Drawing.Point(14, 43);
            this.autoAddCh.Name = "autoAddCh";
            this.autoAddCh.Size = new System.Drawing.Size(151, 17);
            this.autoAddCh.TabIndex = 12;
            this.autoAddCh.Text = "Автодобавление файлов";
            this.autoAddCh.UseVisualStyleBackColor = true;
            this.autoAddCh.CheckedChanged += new System.EventHandler(this.autoAddCh_CheckedChanged);
            // 
            // openInFolderBtn
            // 
            this.openInFolderBtn.Location = new System.Drawing.Point(55, 18);
            this.openInFolderBtn.Name = "openInFolderBtn";
            this.openInFolderBtn.Size = new System.Drawing.Size(118, 23);
            this.openInFolderBtn.TabIndex = 13;
            this.openInFolderBtn.Text = "Показать в папке";
            this.openInFolderBtn.UseVisualStyleBackColor = true;
            this.openInFolderBtn.Click += new System.EventHandler(this.openInFolderBtn_Click);
            // 
            // autorunBox
            // 
            this.autorunBox.Controls.Add(this.randomStartCh);
            this.autorunBox.Controls.Add(this.autorunCh);
            this.autorunBox.Location = new System.Drawing.Point(15, 12);
            this.autorunBox.Name = "autorunBox";
            this.autorunBox.Size = new System.Drawing.Size(227, 71);
            this.autorunBox.TabIndex = 14;
            this.autorunBox.TabStop = false;
            // 
            // autochangeBox
            // 
            this.autochangeBox.Controls.Add(this.autochangeCh);
            this.autochangeBox.Controls.Add(this.randomCh);
            this.autochangeBox.Controls.Add(this.intLb);
            this.autochangeBox.Controls.Add(this.intFormatLb);
            this.autochangeBox.Controls.Add(this.intNum);
            this.autochangeBox.Location = new System.Drawing.Point(248, 12);
            this.autochangeBox.Name = "autochangeBox";
            this.autochangeBox.Size = new System.Drawing.Size(177, 108);
            this.autochangeBox.TabIndex = 15;
            this.autochangeBox.TabStop = false;
            // 
            // windowBox
            // 
            this.windowBox.Controls.Add(this.trayCh);
            this.windowBox.Controls.Add(this.alwaysTrayCh);
            this.windowBox.Controls.Add(this.dontAskWnenExitCh);
            this.windowBox.Controls.Add(this.stickyBoundsCh);
            this.windowBox.Location = new System.Drawing.Point(15, 90);
            this.windowBox.Name = "windowBox";
            this.windowBox.Size = new System.Drawing.Size(202, 120);
            this.windowBox.TabIndex = 16;
            this.windowBox.TabStop = false;
            // 
            // fileBox
            // 
            this.fileBox.Controls.Add(this.searchInChildCh);
            this.fileBox.Controls.Add(this.autoAddCh);
            this.fileBox.Location = new System.Drawing.Point(243, 126);
            this.fileBox.Name = "fileBox";
            this.fileBox.Size = new System.Drawing.Size(182, 70);
            this.fileBox.TabIndex = 17;
            this.fileBox.TabStop = false;
            // 
            // closeTypeBox
            // 
            this.closeTypeBox.Controls.Add(this.minimizeByX);
            this.closeTypeBox.Controls.Add(this.closeByX);
            this.closeTypeBox.Location = new System.Drawing.Point(243, 202);
            this.closeTypeBox.Name = "closeTypeBox";
            this.closeTypeBox.Size = new System.Drawing.Size(182, 65);
            this.closeTypeBox.TabIndex = 18;
            this.closeTypeBox.TabStop = false;
            // 
            // minimizeByX
            // 
            this.minimizeByX.AutoSize = true;
            this.minimizeByX.Location = new System.Drawing.Point(19, 38);
            this.minimizeByX.Name = "minimizeByX";
            this.minimizeByX.Size = new System.Drawing.Size(154, 17);
            this.minimizeByX.TabIndex = 1;
            this.minimizeByX.Text = "Сворачивать по крестику";
            this.minimizeByX.UseVisualStyleBackColor = true;
            this.minimizeByX.CheckedChanged += new System.EventHandler(this.minimizeByX_CheckedChanged);
            // 
            // closeByX
            // 
            this.closeByX.AutoSize = true;
            this.closeByX.Location = new System.Drawing.Point(19, 14);
            this.closeByX.Name = "closeByX";
            this.closeByX.Size = new System.Drawing.Size(145, 17);
            this.closeByX.TabIndex = 0;
            this.closeByX.Text = "Закрывать по крестику";
            this.closeByX.UseVisualStyleBackColor = true;
            this.closeByX.CheckedChanged += new System.EventHandler(this.closeByX_CheckedChanged);
            // 
            // buttonBox
            // 
            this.buttonBox.Controls.Add(this.openInFolderBtn);
            this.buttonBox.Location = new System.Drawing.Point(15, 216);
            this.buttonBox.Name = "buttonBox";
            this.buttonBox.Size = new System.Drawing.Size(222, 51);
            this.buttonBox.TabIndex = 19;
            this.buttonBox.TabStop = false;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 274);
            this.Controls.Add(this.buttonBox);
            this.Controls.Add(this.closeTypeBox);
            this.Controls.Add(this.fileBox);
            this.Controls.Add(this.windowBox);
            this.Controls.Add(this.autochangeBox);
            this.Controls.Add(this.autorunBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Настройки";
            ((System.ComponentModel.ISupportInitialize)(this.intNum)).EndInit();
            this.autorunBox.ResumeLayout(false);
            this.autorunBox.PerformLayout();
            this.autochangeBox.ResumeLayout(false);
            this.autochangeBox.PerformLayout();
            this.windowBox.ResumeLayout(false);
            this.windowBox.PerformLayout();
            this.fileBox.ResumeLayout(false);
            this.fileBox.PerformLayout();
            this.closeTypeBox.ResumeLayout(false);
            this.closeTypeBox.PerformLayout();
            this.buttonBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox autorunCh;
        private System.Windows.Forms.CheckBox randomStartCh;
        private System.Windows.Forms.CheckBox autochangeCh;
        private System.Windows.Forms.CheckBox randomCh;
        private System.Windows.Forms.CheckBox trayCh;
        private System.Windows.Forms.CheckBox alwaysTrayCh;
        private System.Windows.Forms.Label intLb;
        private System.Windows.Forms.Label intFormatLb;
        private System.Windows.Forms.NumericUpDown intNum;
        private System.Windows.Forms.CheckBox dontAskWnenExitCh;
        private System.Windows.Forms.CheckBox stickyBoundsCh;
        private System.Windows.Forms.CheckBox searchInChildCh;
        private System.Windows.Forms.CheckBox autoAddCh;
        private System.Windows.Forms.Button openInFolderBtn;
        private System.Windows.Forms.GroupBox autorunBox;
        private System.Windows.Forms.GroupBox autochangeBox;
        private System.Windows.Forms.GroupBox windowBox;
        private System.Windows.Forms.GroupBox fileBox;
        private System.Windows.Forms.GroupBox closeTypeBox;
        private System.Windows.Forms.RadioButton minimizeByX;
        private System.Windows.Forms.RadioButton closeByX;
        private System.Windows.Forms.GroupBox buttonBox;
    }
}