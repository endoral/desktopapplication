﻿using System;
using System.IO;
using System.Windows.Forms;

namespace DesktopApplication
{
    /// <summary>
    /// Система ведения истории работы приложения.
    /// </summary>
    public static class LogSystem
    {
        #region Fields

        /// <summary>
        /// Имя файла для записи истории работы приложения.
        /// </summary>
        private const string _logName = "Log_system.txt";

        /// <summary>
        /// Путь к папке из которой запускается приложение без имени исполняемого файла приложения.
        /// </summary>
        private static string _applicationPath;

        /// <summary>
        /// Путь к файлу для записи истории работы приложения.
        /// </summary>
        private static string _logPath;

        private static FileStream _fileStream;

        private static StreamWriter _streamWriter;

        #endregion

        #region Methods

        /// <summary>
        /// Путь к лог-файлу.
        /// </summary>
        public static string LogPath
        {
            get
            {
                return _logPath;
            }
        }

        /// <summary>
        /// Установить путь к папке из которой запускается приложение.
        /// </summary>
        /// <param name="path">Путь без исполняемого имени файла.</param>
        public static void SetApplicationPath(string path)
        {
            _applicationPath = path;

            _logPath = _applicationPath + @"\" + _logName;
        }

        private static void init()
        {
            if (_fileStream == null)
            {
                try
                {
                    _fileStream = new FileStream(
                        _applicationPath != null ? _logPath : _logName,
                        FileMode.Append);

                    _streamWriter = new StreamWriter(_fileStream);
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(
                        ex.Message,
                        "Ошибка!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Очистка всех используемых ресурсов.
        /// </summary>
        public static void deinit()
        {
            if (_fileStream != null)
            {
                try
                {
                    _streamWriter.Close();
                    _streamWriter.Dispose();

                    _fileStream.Close();
                    _fileStream.Dispose();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(
                        ex.Message,
                        "Ошибка!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Запись указанных данных в файл.
        /// </summary>
        /// <param name="data">Данные для записи.</param>
        public static void Log(object data)
        {
            init();

            try
            {
                if (data.GetType() == typeof(String) && Convert.ToString(data) == "")
                {
                    _streamWriter.WriteLine(data);
                }
                else
                {
                    _streamWriter.WriteLine(DateTime.Now + ": " + data);
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(
                    ex.Message,
                    "Ошибка!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Пустая строка.
        /// </summary>
        public static void Log()
        {
            Log("");
        }

        /// <summary>
        /// Если условие true записывает указанные данные в файл.
        /// </summary>
        /// <param name="condition">Условие записи.</param>
        /// <param name="data">Данные для записи</param>
        public static void LogIf(bool condition, object data)
        {
            if (condition)
            {
                Log(data);
            }
        }

        /// <summary>
        /// Если указанное условие true записывает в файл первые данные, иначе вторые.
        /// </summary>
        /// <param name="condition">Условие.</param>
        /// <param name="trueData">Первые данные для записи.</param>
        /// <param name="falseData">Вторые данные для записи.</param>
        public static void LogIfElse(bool condition, object trueData, object falseData)
        {
            Log(condition ? trueData : falseData);
        }

        #endregion
    }
}
