﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace DesktopApplication {
    /// <summary>
    /// Настройки программы.
    /// </summary>
    public sealed class SettingsSystem {
        #region Constructors

        /// <summary>
        /// Конструктор.
        /// </summary>
        private SettingsSystem() {
            wallpapers = new List<string>();
            previousWallpapers = new List<int>();
            numIntValue = 60000;
        }

        #endregion

        #region Fields

        /// <summary>
        /// Объект для хранения настроек.
        /// </summary>
        private static SettingsSystem _instance;

        /// <summary>
        /// Настройки.
        /// </summary>
        public static SettingsSystem Instance {
            get {
                if (_instance == null) {
                    _instance = new SettingsSystem();
                }

                return _instance;
            }
        }

        public static readonly string settingsFileName = Application.StartupPath + @"\settings.xml";
        public static readonly string settingsReserveFileName = Application.StartupPath + @"\reserveSettings.xml";

        /// <summary>
        /// В процессе инициализации данных приложения.
        /// </summary>
        public static bool isInitialized = false;

        // - Состояние кнопок вкладки "Навигация"
        public bool btnFirstImageEnabled;
        public bool btnPrevImageEnabled;
        public bool btnRndImageEnabled;
        public bool btnNextImageEnabled;
        public bool btnLastImageEnabled;
        public bool btnSelectedImageEnabled;
        public bool btnChangeImageEnabled;

        /// <summary>
        /// Состояние поля случайного изображения при старте.
        /// </summary>
        public bool checkStartWithRandomImageEnabled;

        /// <summary>
        /// Состояние поля автосмены изображений.
        /// </summary>
        public bool checkAutochangeImageEnabled;

        /// <summary>
        /// Состояние поля случайной смены изображений.
        /// </summary>
        public bool checkRandomImageEnabled;

        /// <summary>
        /// Состояние поля интервала автосмены изображений.
        /// </summary>
        public bool numIntEnabled;

        /// <summary>
        /// Состояние метки заголовка поля интервала автосмены изображений.
        /// </summary>
        public bool labelMarkIntEnabled;

        /// <summary>
        /// Состояние метки описания поля интервала автосмены изображений.
        /// </summary>
        public bool labelMarkIntMinutesEnabled;

        /// <summary>
        /// Статус поля автозапуска.
        /// </summary>
        public bool checkAutorunState;

        /// <summary>
        /// Статус поля случайного изображения при старте.
        /// </summary>
        public bool checkStartWithRandomImageState;

        /// <summary>
        /// Статус поля автосмены изображений.
        /// </summary>
        public bool checkAutochangeImageState;

        /// <summary>
        /// Статус поля случайного порядка смены изображений.
        /// </summary>
        public bool checkRandomImageState;

        /// <summary>
        /// Статус поля минимизации приложения в трей.
        /// </summary>
        public bool checkMinimizeToTrayState;

        /// <summary>
        /// Статус поля постоянного нахождения приложения в трее.
        /// </summary>
        public bool checkAlwaysInTrayState;

        /// <summary>
        /// Задержка автосмены изображений.
        /// </summary>
        public decimal numIntValue;

        /// <summary>
        /// Статус поля поиска файлов в дочерних папках выбранной директории.
        /// </summary>
        public bool searchInSubfoldersState;

        /// <summary>
        /// Статус поля автодобавления файлов в список при добавлении их в папку.
        /// </summary>
        public bool autoAddFilesState;

        /// <summary>
        /// Индекс текущего установленного изображения.
        /// </summary>
        public int currentWallpaperIndex;

        /// <summary>
        /// Путь к текущему изображению в предпросмотре.
        /// </summary>
        public string imageBoxLocation;

        /// <summary>
        /// Путь к выбранной папке с изображениями.
        /// </summary>
        public string folderDialogPath;

        /// <summary>
        /// Индекс текущего изображения в предпросмотре.
        /// </summary>
        public int selectedWallpaper;

        /// <summary>
        /// Имена изображений в выбранной папке.
        /// </summary>
        public List<string> wallpapers;

        /// <summary>
        /// Индексы предыдущих выбранных изображений.
        /// </summary>
        public List<int> previousWallpapers;

        /// <summary>
        /// Состояние окна приложения.
        /// </summary>
        public FormWindowState windowState;

        /// <summary>
        /// Позиция на экране окна приложения.
        /// </summary>
        public Point location;

        /// <summary>
        /// Размер окна приложения.
        /// </summary>
        public Size size;

        /// <summary>
        /// Спрашивать при закрытии приложения.
        /// </summary>
        public bool dontAskWhenCloseState;

        /// <summary>
        /// Сворачивать при нажатии на крестик.
        /// </summary>
        public bool minimizeByX;

        /// <summary>
        /// Статус поля приклеивания приложения к краям экрана.
        /// </summary>
        public bool checkStickyBoundsState;

        #endregion

        #region Methods

        /// <summary>
        /// Сохранение настроек.
        /// </summary>
        public static void SaveData() {
            if (!SettingsSystem.isInitialized) {
                SettingsSystem.Instance.selectedWallpaper = SettingsSystem.Instance.currentWallpaperIndex;

                SettingsSystem.Instance.windowState = mainWnd.Instance.WindowState;

                // Координаты главного окна программы на экране
                SettingsSystem.Instance.location =
                    (mainWnd.Instance.WindowState == FormWindowState.Normal ?
                    mainWnd.Instance.Location : mainWnd.Instance.RestoreBounds.Location);

                // Размер главного окна программы
                SettingsSystem.Instance.size =
                    (mainWnd.Instance.WindowState == FormWindowState.Normal ?
                    mainWnd.Instance.Size : mainWnd.Instance.RestoreBounds.Size);

                try {
                    if (File.Exists(settingsFileName)) {
                        LogSystem.Log("Создание резервной копии предыдущих параметров настройки");
                        File.Copy(settingsFileName, settingsReserveFileName, true);
                    }

                    LogSystem.Log("Создание файлового потока для сериализации и сохранения настроек...");
                    using (Stream uWriter = new FileStream(settingsFileName, FileMode.Create)) {
                        LogSystem.Log("Файловый поток создан для файла: " + settingsFileName + ".");
                        LogSystem.Log("Инициализация сериализатора...");
                        XmlSerializer uSerializer = new XmlSerializer(typeof(SettingsSystem));
                        LogSystem.Log("Сериализация настроек...");
                        uSerializer.Serialize(uWriter, SettingsSystem.Instance);

                        LogSystem.Log("Уничтожение файлового потока...");
                        uWriter.Close();
                        uWriter.Dispose();
                        LogSystem.Log("Настройки успешно сохранены!");
                    }

                    if (File.Exists(settingsReserveFileName)) {
                        LogSystem.Log("Удаление резервной копии предыдущих параметров настройки");
                        File.Delete(settingsReserveFileName);
                    }
                }
                catch (SystemException ex) {
                    LogSystem.Log("Ошибка при сохранении настроек!");
                    LogSystem.Log(ex.Message);
                    MessageBox.Show(
                        ex.Message,
                        "Ошибка записи!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning
                    );
                }
            }
        }

        /// <summary>
        /// Загрузка настроек.
        /// </summary>
        /// <param name="settings">Десериализованный объект с настройками.</param>
        public static void SetInstance(SettingsSystem settings) {
            _instance = settings;
        }

        #endregion
    }
}
