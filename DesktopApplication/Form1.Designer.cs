﻿namespace DesktopApplication
{
    partial class mainWnd
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainWnd));
            this.panelFilesPath = new System.Windows.Forms.Panel();
            this.btnGallery = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.lbFolderPath = new System.Windows.Forms.Label();
            this.lbFileName = new System.Windows.Forms.Label();
            this.lbFileNumber = new System.Windows.Forms.Label();
            this.lbMarkFolderPath = new System.Windows.Forms.Label();
            this.lbMarkFileName = new System.Windows.Forms.Label();
            this.lbMarkFileNumber = new System.Windows.Forms.Label();
            this.btnLoadFiles = new System.Windows.Forms.Button();
            this.lbProgVer = new System.Windows.Forms.Label();
            this.panelWallpaper = new System.Windows.Forms.Panel();
            this.boxWallpaper = new System.Windows.Forms.PictureBox();
            this.dlgOpenFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.trayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.trayContMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.trayContMenuBtnNext = new System.Windows.Forms.ToolStripMenuItem();
            this.trayContMenuBtnPrev = new System.Windows.Forms.ToolStripMenuItem();
            this.trayContMenuBtnRnd = new System.Windows.Forms.ToolStripMenuItem();
            this.trayContMenuSep2 = new System.Windows.Forms.ToolStripSeparator();
            this.trayContMenuBtnToMinimize = new System.Windows.Forms.ToolStripMenuItem();
            this.trayContMenuBtnToNormal = new System.Windows.Forms.ToolStripMenuItem();
            this.trayContMenuSep1 = new System.Windows.Forms.ToolStripSeparator();
            this.trayContMenuBtnExit = new System.Windows.Forms.ToolStripMenuItem();
            this.imgAutoChangeThread = new System.ComponentModel.BackgroundWorker();
            this.btnSelectedImg = new System.Windows.Forms.Button();
            this.btnChangeImg = new System.Windows.Forms.Button();
            this.btnFirstImg = new System.Windows.Forms.Button();
            this.btnLastImg = new System.Windows.Forms.Button();
            this.btnRndImg = new System.Windows.Forms.Button();
            this.btnNextImg = new System.Windows.Forms.Button();
            this.btnPrevImg = new System.Windows.Forms.Button();
            this.navigationPanel = new System.Windows.Forms.Panel();
            this.panelFilesPath.SuspendLayout();
            this.panelWallpaper.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.boxWallpaper)).BeginInit();
            this.trayContMenu.SuspendLayout();
            this.navigationPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelFilesPath
            // 
            this.panelFilesPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelFilesPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelFilesPath.Controls.Add(this.btnGallery);
            this.panelFilesPath.Controls.Add(this.btnSettings);
            this.panelFilesPath.Controls.Add(this.lbFolderPath);
            this.panelFilesPath.Controls.Add(this.lbFileName);
            this.panelFilesPath.Controls.Add(this.lbFileNumber);
            this.panelFilesPath.Controls.Add(this.lbMarkFolderPath);
            this.panelFilesPath.Controls.Add(this.lbMarkFileName);
            this.panelFilesPath.Controls.Add(this.lbMarkFileNumber);
            this.panelFilesPath.Controls.Add(this.btnLoadFiles);
            this.panelFilesPath.Location = new System.Drawing.Point(12, 12);
            this.panelFilesPath.Name = "panelFilesPath";
            this.panelFilesPath.Size = new System.Drawing.Size(608, 88);
            this.panelFilesPath.TabIndex = 14;
            // 
            // btnGallery
            // 
            this.btnGallery.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnGallery.Location = new System.Drawing.Point(504, 32);
            this.btnGallery.Name = "btnGallery";
            this.btnGallery.Size = new System.Drawing.Size(89, 23);
            this.btnGallery.TabIndex = 16;
            this.btnGallery.Text = "Галерея";
            this.btnGallery.UseVisualStyleBackColor = true;
            this.btnGallery.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSettings.Location = new System.Drawing.Point(504, 60);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(89, 23);
            this.btnSettings.TabIndex = 15;
            this.btnSettings.Text = "Настройки";
            this.btnSettings.UseVisualStyleBackColor = true;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // lbFolderPath
            // 
            this.lbFolderPath.AutoSize = true;
            this.lbFolderPath.Location = new System.Drawing.Point(112, 58);
            this.lbFolderPath.Name = "lbFolderPath";
            this.lbFolderPath.Size = new System.Drawing.Size(10, 13);
            this.lbFolderPath.TabIndex = 13;
            this.lbFolderPath.Text = "-";
            this.lbFolderPath.DoubleClick += new System.EventHandler(this.lbFolderPath_DoubleClick);
            // 
            // lbFileName
            // 
            this.lbFileName.AutoSize = true;
            this.lbFileName.Location = new System.Drawing.Point(112, 35);
            this.lbFileName.Name = "lbFileName";
            this.lbFileName.Size = new System.Drawing.Size(10, 13);
            this.lbFileName.TabIndex = 11;
            this.lbFileName.Text = "-";
            this.lbFileName.DoubleClick += new System.EventHandler(this.lbFileName_DoubleClick);
            // 
            // lbFileNumber
            // 
            this.lbFileNumber.AutoSize = true;
            this.lbFileNumber.Location = new System.Drawing.Point(112, 12);
            this.lbFileNumber.Name = "lbFileNumber";
            this.lbFileNumber.Size = new System.Drawing.Size(24, 13);
            this.lbFileNumber.TabIndex = 9;
            this.lbFileNumber.Text = "- / -";
            this.lbFileNumber.DoubleClick += new System.EventHandler(this.lbFileNumber_DoubleClick);
            // 
            // lbMarkFolderPath
            // 
            this.lbMarkFolderPath.AutoSize = true;
            this.lbMarkFolderPath.Location = new System.Drawing.Point(14, 58);
            this.lbMarkFolderPath.Name = "lbMarkFolderPath";
            this.lbMarkFolderPath.Size = new System.Drawing.Size(79, 13);
            this.lbMarkFolderPath.TabIndex = 12;
            this.lbMarkFolderPath.Text = "Путь к папке: ";
            this.lbMarkFolderPath.DoubleClick += new System.EventHandler(this.lbMarkFolderPath_DoubleClick);
            // 
            // lbMarkFileName
            // 
            this.lbMarkFileName.AutoSize = true;
            this.lbMarkFileName.Location = new System.Drawing.Point(14, 35);
            this.lbMarkFileName.Name = "lbMarkFileName";
            this.lbMarkFileName.Size = new System.Drawing.Size(67, 13);
            this.lbMarkFileName.TabIndex = 10;
            this.lbMarkFileName.Text = "Имя файла:";
            this.lbMarkFileName.DoubleClick += new System.EventHandler(this.lbMarkFileName_DoubleClick);
            // 
            // lbMarkFileNumber
            // 
            this.lbMarkFileNumber.AutoSize = true;
            this.lbMarkFileNumber.Location = new System.Drawing.Point(14, 12);
            this.lbMarkFileNumber.Name = "lbMarkFileNumber";
            this.lbMarkFileNumber.Size = new System.Drawing.Size(92, 13);
            this.lbMarkFileNumber.TabIndex = 8;
            this.lbMarkFileNumber.Text = "№ изображения:";
            this.lbMarkFileNumber.DoubleClick += new System.EventHandler(this.lbMarkFileNumber_DoubleClick);
            // 
            // btnLoadFiles
            // 
            this.btnLoadFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadFiles.Location = new System.Drawing.Point(504, 3);
            this.btnLoadFiles.Name = "btnLoadFiles";
            this.btnLoadFiles.Size = new System.Drawing.Size(89, 23);
            this.btnLoadFiles.TabIndex = 0;
            this.btnLoadFiles.Text = "Открыть...";
            this.btnLoadFiles.UseVisualStyleBackColor = true;
            this.btnLoadFiles.Click += new System.EventHandler(this.btnLoadFiles_Click);
            // 
            // lbProgVer
            // 
            this.lbProgVer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbProgVer.Enabled = false;
            this.lbProgVer.Location = new System.Drawing.Point(495, 57);
            this.lbProgVer.Name = "lbProgVer";
            this.lbProgVer.Size = new System.Drawing.Size(110, 13);
            this.lbProgVer.TabIndex = 14;
            this.lbProgVer.Text = "Version: 00.00.00.00";
            this.lbProgVer.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelWallpaper
            // 
            this.panelWallpaper.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelWallpaper.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelWallpaper.Controls.Add(this.boxWallpaper);
            this.panelWallpaper.Location = new System.Drawing.Point(12, 106);
            this.panelWallpaper.Name = "panelWallpaper";
            this.panelWallpaper.Size = new System.Drawing.Size(608, 314);
            this.panelWallpaper.TabIndex = 15;
            // 
            // boxWallpaper
            // 
            this.boxWallpaper.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.boxWallpaper.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.boxWallpaper.InitialImage = null;
            this.boxWallpaper.Location = new System.Drawing.Point(3, 3);
            this.boxWallpaper.Name = "boxWallpaper";
            this.boxWallpaper.Size = new System.Drawing.Size(600, 306);
            this.boxWallpaper.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.boxWallpaper.TabIndex = 0;
            this.boxWallpaper.TabStop = false;
            this.boxWallpaper.WaitOnLoad = true;
            this.boxWallpaper.DoubleClick += new System.EventHandler(this.boxWallpaper_DoubleClick);
            this.boxWallpaper.MouseDown += new System.Windows.Forms.MouseEventHandler(this.boxWallpaper_MouseDown);
            this.boxWallpaper.MouseMove += new System.Windows.Forms.MouseEventHandler(this.boxWallpaper_MouseMove);
            this.boxWallpaper.MouseUp += new System.Windows.Forms.MouseEventHandler(this.boxWallpaper_MouseUp);
            // 
            // dlgOpenFolder
            // 
            this.dlgOpenFolder.Description = "Папка с изображениями формата *.jpg";
            this.dlgOpenFolder.ShowNewFolderButton = false;
            // 
            // trayIcon
            // 
            this.trayIcon.ContextMenuStrip = this.trayContMenu;
            this.trayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("trayIcon.Icon")));
            this.trayIcon.Text = "Desktop Application";
            this.trayIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.trayIcon_MouseClick);
            this.trayIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.trayIcon_MouseDoubleClick);
            // 
            // trayContMenu
            // 
            this.trayContMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trayContMenuBtnNext,
            this.trayContMenuBtnPrev,
            this.trayContMenuBtnRnd,
            this.trayContMenuSep2,
            this.trayContMenuBtnToMinimize,
            this.trayContMenuBtnToNormal,
            this.trayContMenuSep1,
            this.trayContMenuBtnExit});
            this.trayContMenu.Name = "trayContMenu";
            this.trayContMenu.Size = new System.Drawing.Size(179, 148);
            // 
            // trayContMenuBtnNext
            // 
            this.trayContMenuBtnNext.Name = "trayContMenuBtnNext";
            this.trayContMenuBtnNext.Size = new System.Drawing.Size(178, 22);
            this.trayContMenuBtnNext.Text = "Следующие обои";
            this.trayContMenuBtnNext.Click += new System.EventHandler(this.trayContMenuBtnNext_Click);
            // 
            // trayContMenuBtnPrev
            // 
            this.trayContMenuBtnPrev.Name = "trayContMenuBtnPrev";
            this.trayContMenuBtnPrev.Size = new System.Drawing.Size(178, 22);
            this.trayContMenuBtnPrev.Text = "Предыдущие обои";
            this.trayContMenuBtnPrev.Click += new System.EventHandler(this.trayContMenuBtnPrev_Click);
            // 
            // trayContMenuBtnRnd
            // 
            this.trayContMenuBtnRnd.Name = "trayContMenuBtnRnd";
            this.trayContMenuBtnRnd.Size = new System.Drawing.Size(178, 22);
            this.trayContMenuBtnRnd.Text = "Случайные обои";
            this.trayContMenuBtnRnd.Click += new System.EventHandler(this.trayContMenuBtnRnd_Click);
            // 
            // trayContMenuSep2
            // 
            this.trayContMenuSep2.Name = "trayContMenuSep2";
            this.trayContMenuSep2.Size = new System.Drawing.Size(175, 6);
            // 
            // trayContMenuBtnToMinimize
            // 
            this.trayContMenuBtnToMinimize.Name = "trayContMenuBtnToMinimize";
            this.trayContMenuBtnToMinimize.Size = new System.Drawing.Size(178, 22);
            this.trayContMenuBtnToMinimize.Text = "Свернуть";
            this.trayContMenuBtnToMinimize.Click += new System.EventHandler(this.trayContMenuBtnToMinimize_Click);
            // 
            // trayContMenuBtnToNormal
            // 
            this.trayContMenuBtnToNormal.Name = "trayContMenuBtnToNormal";
            this.trayContMenuBtnToNormal.Size = new System.Drawing.Size(178, 22);
            this.trayContMenuBtnToNormal.Text = "Развернуть";
            this.trayContMenuBtnToNormal.Visible = false;
            this.trayContMenuBtnToNormal.Click += new System.EventHandler(this.trayConMenuBtnNormalState_Click);
            // 
            // trayContMenuSep1
            // 
            this.trayContMenuSep1.Name = "trayContMenuSep1";
            this.trayContMenuSep1.Size = new System.Drawing.Size(175, 6);
            // 
            // trayContMenuBtnExit
            // 
            this.trayContMenuBtnExit.Name = "trayContMenuBtnExit";
            this.trayContMenuBtnExit.Size = new System.Drawing.Size(178, 22);
            this.trayContMenuBtnExit.Text = "Выход";
            this.trayContMenuBtnExit.Click += new System.EventHandler(this.trayContMenuBtnExit_Click);
            // 
            // imgAutoChangeThread
            // 
            this.imgAutoChangeThread.WorkerSupportsCancellation = true;
            this.imgAutoChangeThread.DoWork += new System.ComponentModel.DoWorkEventHandler(this.imgAutoChangeThread_DoWork);
            // 
            // btnSelectedImg
            // 
            this.btnSelectedImg.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSelectedImg.Enabled = false;
            this.btnSelectedImg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSelectedImg.Location = new System.Drawing.Point(207, 41);
            this.btnSelectedImg.Name = "btnSelectedImg";
            this.btnSelectedImg.Size = new System.Drawing.Size(89, 23);
            this.btnSelectedImg.TabIndex = 8;
            this.btnSelectedImg.Text = "К текущему";
            this.btnSelectedImg.UseVisualStyleBackColor = true;
            this.btnSelectedImg.Click += new System.EventHandler(this.btnSelectedImg_Click);
            // 
            // btnChangeImg
            // 
            this.btnChangeImg.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnChangeImg.Enabled = false;
            this.btnChangeImg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnChangeImg.Location = new System.Drawing.Point(320, 41);
            this.btnChangeImg.Name = "btnChangeImg";
            this.btnChangeImg.Size = new System.Drawing.Size(89, 23);
            this.btnChangeImg.TabIndex = 7;
            this.btnChangeImg.Text = "Установить";
            this.btnChangeImg.UseVisualStyleBackColor = true;
            this.btnChangeImg.Click += new System.EventHandler(this.btnChangeImg_Click);
            // 
            // btnFirstImg
            // 
            this.btnFirstImg.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnFirstImg.Enabled = false;
            this.btnFirstImg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnFirstImg.Location = new System.Drawing.Point(65, 8);
            this.btnFirstImg.Name = "btnFirstImg";
            this.btnFirstImg.Size = new System.Drawing.Size(89, 23);
            this.btnFirstImg.TabIndex = 2;
            this.btnFirstImg.Text = "Первое";
            this.btnFirstImg.UseVisualStyleBackColor = true;
            this.btnFirstImg.Click += new System.EventHandler(this.btnFirstImg_Click);
            // 
            // btnLastImg
            // 
            this.btnLastImg.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLastImg.Enabled = false;
            this.btnLastImg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnLastImg.Location = new System.Drawing.Point(464, 8);
            this.btnLastImg.Name = "btnLastImg";
            this.btnLastImg.Size = new System.Drawing.Size(89, 23);
            this.btnLastImg.TabIndex = 6;
            this.btnLastImg.Text = "Последнее";
            this.btnLastImg.UseVisualStyleBackColor = true;
            this.btnLastImg.Click += new System.EventHandler(this.btnLastImg_Click);
            // 
            // btnRndImg
            // 
            this.btnRndImg.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnRndImg.Enabled = false;
            this.btnRndImg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnRndImg.Location = new System.Drawing.Point(264, 8);
            this.btnRndImg.Name = "btnRndImg";
            this.btnRndImg.Size = new System.Drawing.Size(89, 23);
            this.btnRndImg.TabIndex = 4;
            this.btnRndImg.Text = "Случайное";
            this.btnRndImg.UseVisualStyleBackColor = true;
            this.btnRndImg.Click += new System.EventHandler(this.btnRndImg_Click);
            // 
            // btnNextImg
            // 
            this.btnNextImg.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnNextImg.Enabled = false;
            this.btnNextImg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnNextImg.Location = new System.Drawing.Point(369, 8);
            this.btnNextImg.Name = "btnNextImg";
            this.btnNextImg.Size = new System.Drawing.Size(89, 23);
            this.btnNextImg.TabIndex = 5;
            this.btnNextImg.Text = "Следующее";
            this.btnNextImg.UseVisualStyleBackColor = true;
            this.btnNextImg.Click += new System.EventHandler(this.btnNextImg_Click);
            // 
            // btnPrevImg
            // 
            this.btnPrevImg.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnPrevImg.Enabled = false;
            this.btnPrevImg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPrevImg.Location = new System.Drawing.Point(160, 8);
            this.btnPrevImg.Name = "btnPrevImg";
            this.btnPrevImg.Size = new System.Drawing.Size(89, 23);
            this.btnPrevImg.TabIndex = 3;
            this.btnPrevImg.Text = "Предыдущее";
            this.btnPrevImg.UseVisualStyleBackColor = true;
            this.btnPrevImg.Click += new System.EventHandler(this.btnPrevImg_Click);
            this.btnPrevImg.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnPrevImg_MouseUp);
            // 
            // navigationPanel
            // 
            this.navigationPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.navigationPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.navigationPanel.Controls.Add(this.btnNextImg);
            this.navigationPanel.Controls.Add(this.btnSelectedImg);
            this.navigationPanel.Controls.Add(this.lbProgVer);
            this.navigationPanel.Controls.Add(this.btnPrevImg);
            this.navigationPanel.Controls.Add(this.btnChangeImg);
            this.navigationPanel.Controls.Add(this.btnRndImg);
            this.navigationPanel.Controls.Add(this.btnLastImg);
            this.navigationPanel.Controls.Add(this.btnFirstImg);
            this.navigationPanel.Location = new System.Drawing.Point(12, 426);
            this.navigationPanel.Name = "navigationPanel";
            this.navigationPanel.Size = new System.Drawing.Size(610, 74);
            this.navigationPanel.TabIndex = 16;
            // 
            // mainWnd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 512);
            this.Controls.Add(this.navigationPanel);
            this.Controls.Add(this.panelWallpaper);
            this.Controls.Add(this.panelFilesPath);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(650, 550);
            this.Name = "mainWnd";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Desktop Application";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWnd_FormClosing);
            this.Load += new System.EventHandler(this.MainWnd_Load);
            this.Resize += new System.EventHandler(this.mainWnd_Resize);
            this.panelFilesPath.ResumeLayout(false);
            this.panelFilesPath.PerformLayout();
            this.panelWallpaper.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.boxWallpaper)).EndInit();
            this.trayContMenu.ResumeLayout(false);
            this.navigationPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelFilesPath;
        private System.Windows.Forms.Label lbMarkFolderPath;
        private System.Windows.Forms.Label lbMarkFileName;
        private System.Windows.Forms.Label lbMarkFileNumber;
        private System.Windows.Forms.Button btnLoadFiles;
        private System.Windows.Forms.Panel panelWallpaper;
        private System.Windows.Forms.PictureBox boxWallpaper;
        private System.Windows.Forms.FolderBrowserDialog dlgOpenFolder;
        private System.Windows.Forms.Label lbFolderPath;
        private System.Windows.Forms.Label lbFileName;
        private System.Windows.Forms.Label lbFileNumber;
        private System.Windows.Forms.ContextMenuStrip trayContMenu;
        private System.Windows.Forms.ToolStripMenuItem trayContMenuBtnExit;
        private System.Windows.Forms.ToolStripMenuItem trayContMenuBtnNext;
        private System.Windows.Forms.ToolStripMenuItem trayContMenuBtnPrev;
        private System.Windows.Forms.ToolStripMenuItem trayContMenuBtnRnd;
        private System.Windows.Forms.ToolStripSeparator trayContMenuSep2;
        private System.Windows.Forms.ToolStripMenuItem trayContMenuBtnToNormal;
        private System.Windows.Forms.ToolStripSeparator trayContMenuSep1;
        private System.Windows.Forms.ToolStripMenuItem trayContMenuBtnToMinimize;
        private System.Windows.Forms.Label lbProgVer;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Button btnSelectedImg;
        private System.Windows.Forms.Button btnChangeImg;
        private System.Windows.Forms.Button btnFirstImg;
        private System.Windows.Forms.Button btnLastImg;
        private System.Windows.Forms.Button btnRndImg;
        private System.Windows.Forms.Button btnNextImg;
        private System.Windows.Forms.Button btnPrevImg;
        public System.ComponentModel.BackgroundWorker imgAutoChangeThread;
        public System.Windows.Forms.NotifyIcon trayIcon;
        private System.Windows.Forms.Button btnGallery;
        private System.Windows.Forms.Panel navigationPanel;

    }
}

