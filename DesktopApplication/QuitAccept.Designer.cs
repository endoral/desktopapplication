﻿namespace DesktopApplication
{
    partial class QuitAccept
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.yesBtn = new System.Windows.Forms.Button();
            this.noBtn = new System.Windows.Forms.Button();
            this.dontShowCheck = new System.Windows.Forms.CheckBox();
            this.questionLabel = new System.Windows.Forms.Label();
            this.iconImg = new System.Windows.Forms.PictureBox();
            this.backingImg = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.iconImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backingImg)).BeginInit();
            this.SuspendLayout();
            // 
            // yesBtn
            // 
            this.yesBtn.Location = new System.Drawing.Point(177, 105);
            this.yesBtn.Name = "yesBtn";
            this.yesBtn.Size = new System.Drawing.Size(80, 25);
            this.yesBtn.TabIndex = 1;
            this.yesBtn.Text = "Да";
            this.yesBtn.UseVisualStyleBackColor = true;
            this.yesBtn.Click += new System.EventHandler(this.yesBtn_Click);
            // 
            // noBtn
            // 
            this.noBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.noBtn.Location = new System.Drawing.Point(269, 105);
            this.noBtn.Name = "noBtn";
            this.noBtn.Size = new System.Drawing.Size(80, 25);
            this.noBtn.TabIndex = 2;
            this.noBtn.Text = "Нет";
            this.noBtn.UseVisualStyleBackColor = true;
            this.noBtn.Click += new System.EventHandler(this.noBtn_Click);
            // 
            // dontShowCheck
            // 
            this.dontShowCheck.AutoSize = true;
            this.dontShowCheck.Location = new System.Drawing.Point(12, 112);
            this.dontShowCheck.Name = "dontShowCheck";
            this.dontShowCheck.Size = new System.Drawing.Size(144, 17);
            this.dontShowCheck.TabIndex = 3;
            this.dontShowCheck.Text = "Больше не показывать";
            this.dontShowCheck.UseVisualStyleBackColor = true;
            this.dontShowCheck.CheckedChanged += new System.EventHandler(this.dontShowCheck_CheckedChanged);
            // 
            // questionLabel
            // 
            this.questionLabel.AutoSize = true;
            this.questionLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.questionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.questionLabel.Location = new System.Drawing.Point(60, 39);
            this.questionLabel.Name = "questionLabel";
            this.questionLabel.Size = new System.Drawing.Size(277, 15);
            this.questionLabel.TabIndex = 5;
            this.questionLabel.Text = "Вы уверены что хотите закрыть приложение?";
            // 
            // iconImg
            // 
            this.iconImg.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.iconImg.BackgroundImage = global::DesktopApplication.Properties.Resources._109_AllAnnotations_Help_32x32_72;
            this.iconImg.Location = new System.Drawing.Point(22, 30);
            this.iconImg.Name = "iconImg";
            this.iconImg.Size = new System.Drawing.Size(32, 32);
            this.iconImg.TabIndex = 6;
            this.iconImg.TabStop = false;
            // 
            // backingImg
            // 
            this.backingImg.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.backingImg.Location = new System.Drawing.Point(-2, 0);
            this.backingImg.Name = "backingImg";
            this.backingImg.Size = new System.Drawing.Size(362, 91);
            this.backingImg.TabIndex = 4;
            this.backingImg.TabStop = false;
            // 
            // QuitAccept
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 141);
            this.Controls.Add(this.iconImg);
            this.Controls.Add(this.questionLabel);
            this.Controls.Add(this.backingImg);
            this.Controls.Add(this.dontShowCheck);
            this.Controls.Add(this.noBtn);
            this.Controls.Add(this.yesBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "QuitAccept";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Подтверждение закрытия";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.QuitAccept_FormClosing);
            this.Load += new System.EventHandler(this.QuitAccept_Load);
            ((System.ComponentModel.ISupportInitialize)(this.iconImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backingImg)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button yesBtn;
        private System.Windows.Forms.Button noBtn;
        private System.Windows.Forms.CheckBox dontShowCheck;
        private System.Windows.Forms.PictureBox backingImg;
        private System.Windows.Forms.Label questionLabel;
        private System.Windows.Forms.PictureBox iconImg;
    }
}