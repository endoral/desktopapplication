﻿using System;
using System.Windows.Forms;

namespace DesktopApplication
{
    public partial class QuitAccept : Form
    {
        #region Constructors

        public QuitAccept()
        {
            InitializeComponent();
        }

        #endregion

        #region Fields

        private static QuitAccept _instance;

        private DialogResult _result;

        #endregion

        #region Methods

        public static DialogResult ShowMessage()
        {
            if (_instance == null)
            {
                _instance = new QuitAccept();
            }

            _instance._result = DialogResult.None;
            _instance.ShowDialog();

            return _instance._result;
        }

        #endregion

        #region Events

        private void QuitAccept_Load(object sender, EventArgs e)
        {
            Utils.SetCrossActive(this.Handle, false);
        }

        private void QuitAccept_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_result == DialogResult.None)
            {
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;
            }
        }

        private void yesBtn_Click(object sender, EventArgs e)
        {
            _result = DialogResult.Yes;
            this.Close();
        }

        private void noBtn_Click(object sender, EventArgs e)
        {
            _result = DialogResult.No;
            this.Close();
        }

        private void dontShowCheck_CheckedChanged(object sender, EventArgs e)
        {
            SettingsSystem.Instance.dontAskWhenCloseState = dontShowCheck.Checked;
        }

        #endregion
    }
}
